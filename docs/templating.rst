.. _templating:

Templating
==========

Cutlass doesn't come with a system for generating markup and it doesn't have
any opinion about which one you should use. Use any good one that works with
Python. Here are a few to consider:

- `Jinja2 <http://jinja.pocoo.org/docs/>`_ 
- `Mustache <http://mustache.github.com/>`_ using `pystache <https://github.com/defunkt/pystache>`_
- `Mako <http://www.makotemplates.org/>`_
- `Chameleon <http://chameleon.readthedocs.org/>`_
- `Django <https://docs.djangoproject.com/en/dev/ref/templates/api/#configuring-the-template-system-in-standalone-mode>`_
- `Genshi <http://genshi.edgewall.org/>`_
- even Python's :py:meth:`str.format <str.format>` (if you are careful to escape
  inputs)

Not sure how to choose? To start with, use something which is easy to set up
and has syntax which looks good to you. There has historically been a lot of
talk about speed, but most popular templating engines are "fast enough" and can
be supplemented with liberal caching and use of static files. Nowadays, most
web apps spend far more time waiting on database connections and file downloads 
than on template substitutions. 

Here is an older article by `Catherine Devlin comparing some template engines on syntax and error quality
<http://catherinedevlin.pythoneers.com/cookoff.html>`_ 
(blog post `here <http://catherinedevlin.blogspot.com/2010/04/templating-engine-cookoff.html>`_, 
code `here <https://code.launchpad.net/~catherine-devlin/templatecookoff/trunk>`_).
