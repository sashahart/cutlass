Glossary
--------

.. glossary::
    :sorted:

    bytestring
        An instance of Python's builtin :py:class:`bytes` type, containing
        a sequence of bytes. This is different from normal strings, which
        contain human-readable characters. While both might seem readable,
        a bytestring like b"foo" is raw data in a possibly unknown encoding,
        while a string like "foo" is actual characters representing a word. 
        ``b"foo".decode('utf-8')`` returns ``"foo"`` (assuming you know the
        bytestring is encoded as UTF-8), while ``"foo".encode('utf-8')``
        returns ``b"foo"`` (explicitly encoding it as utf-8). 
        But results vary depending on the encoding::

            >>> "foo".encode("utf-32")
            b'\xff\xfe\x00\x00f\x00\x00\x00o\x00\x00\x00o\x00\x00\x00'

    callable
        Any Python object which can be called, e.g.::

            obj()  # if this works, then obj is callable
        
        The classic example is a function, but objects with
        :py:meth:`__call__<object.__call__>` methods are also callables, as are
        class objects.

    CGI
        Ancient interface allowing any sort of program (not just Python) to
        talk to a server. This was often used with shell scripts, Perl
        programs, C programs, etc. Now it is not used much at all.
        Despite many differences, many details of :term:`WSGI` are drawn from
        CGI; for example, the :term:`environ` dict passed to a :term:`WSGI app`
        resembles the os.environ dict-like accessed by Python CGI scripts,
        sharing many keys like REQUEST_METHOD and PATH_INFO (CGI keys are
        typically capitalized in the WSGI environ).

    close()
        A method which may or may not be present on a :term:`response iterable`. 
        If present, it must be called by the :term:`WSGI server`.
        The purpose of this is to provide a hook for freeing up resources. 
        See also Graham Dumpleton's 
        "`Obligations for calling close() on the iterable returned by a WSGI 
        application 
        <http://blog.dscpl.com.au/2012/10/obligations-for-calling-close-on.html>`_."

    CSRF
    XSRF
        Cross site request forgery.

        There are MANY ways to use one site to force someone's browser to
        submit arbitrary data to a different site (img tags, css links,
        Javascript which submits a form, etc.) Whatever the technique, this
        typically causes browsers to automatically send e.g. login cookies, as
        for any other request. This becomes an issue when a browser is forced
        to make a request to some site to which a user is authenticated. Many
        sites see a login cookie and just do whatever was requested, just as if
        the logged-in user requested it. So people can make users' browsers do
        things on their behalf, without having to learn a single password or
        session id. That's CSRF, and preventing it is a responsibility of
        anyone writing web apps.
        
        For more introductory information on CSRF, see 
        `Wikipedia's article on CSRF <http://en.wikipedia.org/wiki/Cross-site_request_forgery>`_ and
        `OWASP's article on CSRF
        <https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)>`_.

    environ
        The first argument a WSGI server passes to a WSGI application, this is
        a dict containing information on an HTTP request. An environ dict
        contains CGI style keys like 'REQUEST_ADDR' and 'HTTP_COOKIE' as well
        as some WSGI-specific keys like :term:`'wsgi.input' <wsgi.input>`.

    exc_info
        The third, optional argument a WSGI app can pass to
        :term:`start_response`. When there is an error that leads to some need
        to change headers, a tuple returned by :func:`sys.exc_info()
        <sys.exc_info>` can be passed to give the server an opportunity to
        abort in the event that it has already sent some header data, otherwise
        correct headers if possible.
        start_response should not be called more than once without providing
        the exc_info argument. In typical practice, it could be argued that
        there's not a lot of point in doing this when you can just let the
        exception bubble up to the server (which will typically handle it
        if it's worth anything), or issue a 500 response with just one call to
        start_response; frameworks often do things like this.

    framework
        A superstructure of code on the application side, presenting a unified
        API or collection of APIs which abstract out lower-level concerns like
        HTTP, markup generation, database interactions, auth, etc. Frameworks
        often amount to generic applications and prewired bundles of components
        ready for you to configure, extend with plugins, or customize along
        predetermined lines.

    handler
        A bit of code which looks at an HTTP request (in some form) and
        coordinates the generation of an HTTP response (in some form), doing
        whatever else in between. This term is often used to neutrally describe
        this task without getting caught up in controversies like what constitutes
        a 'view' or 'controller' in HTTP land. For example, the role of
        a function wrapped by :py:class:`@WSGI<cutlass.wsgi.WSGI>` is to handle
        requests of a certain kind, but the part this plays in the bigger
        system is an open question up to the user, and the generic notion of
        a handler does not imply any specific call signature, as e.g.
        :term:`WSGI app` does.

    iterable
        Any Python object which can be iterated over, e.g.::

            for item in obj:  # if this works, then obj is iterable
                print(item)

        This could be a :term:`list`, tuple, an object implementing the
        iterator protocol, a :term:`generator expression`, the result of
        calling :py:func:`iter() <iter>` on one of these, etc.

        There are two gotchas with iterables that are particularly notable in
        a WSGI context. The first is that strings are iterables, so they can be
        returned from a raw WSGI app without an apparent error; but this will
        be very slow, since one character will be yielded at a time. The second
        is that some iterables (like :term:`wsgi.input`) are 'consumed' as
        items are yielded from them.

    mod_python
        An old module for Apache for making web apps in Python, now completely
        deprecated and superceded by :term:`mod_wsgi`. Don't use it now.

    mod_wsgi
        An module for the popular `Apache HTTP daemon
        <http://httpd.apache.org/>`_ which allows it to serve :term:`WSGI
        apps<WSGI app>` (i.e., act as a :term:`WSGI server`). This is one of
        the oldest, most mature and featureful WSGI server implementations
        available and has played a large role in the development of WSGI
        itself.
        For more about mod_wsgi, see the `mod_wsgi page
        <http://code.google.com/p/modwsgi/>`_. (There is also a mod_wsgi for
        nginx which is unrelated.)

    native string
        This is a term defined by the WSGI PEP 3333.
        In Python 2, a native string is of a type named str. In Python 3,
        a native string is also of a type named str. Despite having the same
        name, these are different types: str in Python 2 acts like bytes
        in Python 3, while str in Python 3 acts like unicode in Python 2. 
        In any case, the characters represented have to be encodeable as
        latin-1 to be 'native strings'.

        WSGI says that the stuff in an environ and the stuff passed to
        :term:`start_response` must be native strings (i.e., of a type named
        'str' but encodeable as latin-1 bytestrings). By contrast, each item
        read from wsgi.input or written with :term:`write()<write>` or as part
        of the :term:`response iterable` must be a :term:`bytestring`.
        This is basically a compromise adapting the earlier PEP 333 to specify
        more clearly what to do with Python 3's changed encoding behavior.

    path traversal
        A :term:`routing` technique where bits of the :term:`request path` 
        (in the most common case) are popped off and used to move through
        a tree of objects to find a match. This technique is used heavily in
        Zope and its derivatives, as well as CherryPy and several frameworks of
        similar vintage. It bears similarities to having WSGI apps dispatch to
        each other, or keeping routing information in a tree data structure.

    PEP 333
        The document originally defining :term:`WSGI`; superceded by :term:`PEP
        3333`.

    PEP 3333
        The document updating :term:`WSGI` for support of Python 3, mostly by
        detailing how to deal with string types. Check it out here: :pep:`3333`

    request method
        The part of an HTTP request representing the 'verb' of what the client
        is trying to do. Available from environ as environ['REQUEST_METHOD'].
    
    request path
        The part of an HTTP request representing the 'noun' of what the client
        is trying to do. Available from environ as environ['PATH_INFO'].

    response object
        Some kind of object used to pass around and/or work on data that will
        ultimately be rendered and sent by the server as a raw HTTP response.
        Many frameworks and web APIs define their own response objects and
        pass them around in different ways.
    
    routing
        Finding a bit of code to handle a request; deciding which handler
        should be called. This is most usually based on the request method and
        request path, using techniques like a list of regular expressions or
        a :term:`path traversal` tree.

    start_response
        A callable provided to the :term:`WSGI application object` by the
        :term:`WSGI server` so that the application can communicate HTTP header
        information to the server before sending out data for the HTTP response
        body. The data is buffered or queued by the :term:`WSGI server` until
        it actually has data to send (whether from the :term:`response
        iterable` or calls to :term:`write()<write>`). For more on
        start_response, see :pep:`3333#the-start-response-callable`.

    threadlocals
        another name for :py:class:`threading.local`. These are magical values
        which look different from different threads, allowing them to be
        changed inside each thread without affecting the state of other
        concurrent threads. 

    WSGI
        Short name for the Python Web Server Gateway Interface. :pep:`3333`
        defines a way for a part of a web server (the :term:`WSGI server`) to
        talk to a Python web application (the :term:`WSGI application object`),
        with this conversation possibly passing through intermediate layers
        which modify the conversation between app and server (:term:`WSGI
        middleware`). This is the most common way to deploy Python web apps,
        supported by nearly all of Python's actively-used web frameworks and
        libraries.
        
    WSGI application object
    WSGI app
        An entry point to a Python web application that talks to a 
        :term:`WSGI server`. 
        It is the Python :term:`callable` invoked by the server to process an
        HTTP request and produce an HTTP response.

    WSGI server
        Anything which calls a :term:`WSGI application object`, passing an
        :term:`environ` and a :term:`start_response`, receiving the
        :term:`response iterable`, and sending headers and data when some data
        has yielded from the iterable (or, unfortunately, when :term:`write()
        <write>` is called). This might be part of a web server, app server or
        a gateway or even an adapter used to provide a WSGI interface on top of
        another environment like CGI. It may also actually refer to
        a :term:`WSGI middleware`. (In WSGI, the app is not supposed to know or
        care what is acting as the server, and the server is not supposed to
        know or care what is acting as the app.)

    WSGI middleware

        Wrapper code which intercepts and processes request data coming into
        WSGI applications and/or processes response data coming out of them, by
        appearing to the server as applications and to the wrapped application
        as the server. 
        
        This is hard to do properly, and it isn't always a good idea. Often,
        it is only useful to augment a server with content-neutral capabilities
        like request logging, where the server does not provide a more
        efficient version of these.
        
        In the following example, some_middleware can be called to proxy for
        real_app; it messes with the environ on the way in, to fool the app,
        and reverses each piece of the original app's output on the way out::

            def real_app(environ, start_response):
                start_response("200 OK", [])
                return ["Everything", "looks", "normal"]

            def annoying_middleware(environ, start_response):
                environ['REMOTE_ADDR'] = '127.0.0.1'
                # real_app calls start_response itself, but we could also
                # provide a fake start_response and call it here to intercept
                iterable = real_app(environ, start_response)
                # yield each item reversed, to mess things up
                return (item[::-1] for item in iterable)

        Real middleware is often exposed as a decorator or object, so it's
        easier to layer it over arbitrary objects (including objects which
        already have one or more layers of middleware applied).

    response iterable
        An object returned by a :term:`WSGI application object` which provides
        chunks of data (as :term:`bytestrings<bytestring>`) for the HTTP
        response body. This is just a special case of an :term:`iterable`.

    write
        A callable returned by :term:`start_response`, provided by the
        :term:`WSGI server` to support old imperative APIs for sending HTTP
        response body data. For more, see :pep:`3333#the-write-callable`.
        Please don't use it. You don't need it and depending on it makes things
        horribly complicated for everyone else.

    request object
        Some kind of object used to encapsulate (and possibly also expose an
        API for processing) HTTP response data, usually acquired through a dict
        like CGI os.environ or :term:`WSGI environ <environ>`. Frameworks often
        preprocess and expose HTTP request data in the form of these request
        objects - usually in globals, :term:`threadlocals` or superclass
        methods, sometimes as arguments automatically supplied to a user's
        'views' or 'controllers'.

    wsgi.input
        A key in an :term:`environ` dict (i.e., environ['wsgi.input']) which
        provides access to a file-like object from which a request body can be
        read; this would be where to get things like form POST data and
        uploaded files.

        This is analogous to the role of :py:class:`sys.stdin` in Python
        :term:`CGI` scripts. But where wsgi.input is given by the :term:`WSGI
        server` to the specific invocation of a :term:`WSGI app`, sys.stdin is
        global and using it with WSGI is likely to cause problems.

    wsgi.errors
        A key in an :term:`environ` dict (i.e., environ['wsgi.errors']) which
        provides access to an output stream where error messages can be sent.

        This is analogous to the role of :py:class:`sys.stderr` in Python
        :term:`CGI` scripts. But whereas wsgi.errors is given by the
        :term:`WSGI server` to the specific invocation of a :term:`WSGI app`,
        sys.stderr is global and using it with WSGI is likely to cause
        problems.

    XSS
        Cross-site scripting. In a nutshell, it involves injecting markup,
        Javascript, etc. into a web page served by someone else's server in
        order to smuggle malicious content to clients as if it were from
        a trusted source. XSS makes it critically important that any website
        appropriately escape user input before including it in sent responses.
        It is very easy to forget to do this or to do it incorrectly.

        For more information, see `Wikipedia's article on XSS
        <http://en.wikipedia.org/wiki/Cross-site_scripting>`_ and `OWASP's
        article on XSS
        <https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)>`_.
