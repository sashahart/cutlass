.. _forms:

Forms
=====

At present, Cutlass doesn't do a lot to automate form processing. 

Here are some third-party libraries for doing forms:

    - `WTForms <http://wtforms.simplecodes.com/>`_ is a pretty well-developed
      form processing library with a style somewhat similar to Django forms.
    - `FormAlchemy <http://docs.formalchemy.org/>`_ (if you are using SQLAlchemy)
    - `FormEncode <http://www.formencode.org/en/latest/index.html>`_
    - `Deform <http://docs.pylonsproject.org/projects/deform/dev/>`_
    - even roll your own with :py:class:`cgi.FieldStorage` or similar (although
      this might be masochistic)

If you aren't too happy using one of these or rolling your own, stay tuned
- Cutlass will soon provide some simple standalone tools for making form
handlers (in the characteristic modular style).
