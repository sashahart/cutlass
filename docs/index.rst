.. _index:

Cutlass Documentation
=====================

Cutlass is a Python (2.7/3.x) library for making web apps.

It's a small, carefully-designed set of components which do basic jobs usually
done by a framework, without needing to be used together. Dependencies,
internal coupling, automatic behavior and magic are kept to a minimum. Cutlass'
components snap together flexibly with each other and anything else that uses
:term:`WSGI`, including most Python frameworks as well as best-of-breed
libraries like `WebOb <http://webob.org/>`_, `Bottle <http://bottlepy.org/>`_
and `Werkzeug <http://werkzeug.pocoo.org/>`_. (You don't have to know about
WSGI, though you can easily work at that level if you need to.)

This design lets you get started quickly, completely understand and control
what your app is doing, keep overhead down, and tailor your architecture and
components to your tastes and tasks.

Cutlass doesn't couple to particular systems for
:doc:`templating <templating>`,
:doc:`storage <storage>`,
:doc:`auth <auth>` or :doc:`sessions <sessions>`,
so choose whichever tools you want for those purposes.

For those who are curious, here is an explanation :doc:`about the name <about_the_name>`.


Table of Contents
-----------------

.. toctree::
    :maxdepth: 2

    wsgi
    routing
    request
    response
    transform
    config
