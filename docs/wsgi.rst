cutlass.wsgi
============

.. automodule:: wsgi

This standalone module provides tools to automate the creation of :term:`WSGI
"application objects" <WSGI application object>` which are used to handle
requests - either as modular parts of a bigger app, or as the entry point run
directly by the :term:`WSGI server`. These deal with error handling and a few
other details of the standard.

This module is optional, as in not required or used by any other module in
Cutlass. As far as the rest of Cutlass is concerned, any kind of WSGI app is
equivalent, allowing you to write raw WSGI apps, use another framework, or
whatever, and either pass them to the server or wire them up into bigger WSGI
apps using tools like :py:class:`~routing.Dispatcher` and
:py:class:`~routing.Cascade`.

Background
----------

As of this writing, :term:`WSGI` is the *de facto* standard for running Python
web applications. If you want to write a Python web app and don't have some
specific need to use a non-WSGI server (e.g. Zope, Twisted, Tornado,
mod_python, old-school CGI) - which you probably don't - then a WSGI server is
what you'll want to use. This will give you a wide choice of good servers that
will all work with your code.

The bare minimum to write an app for a :term:`WSGI server` is to
make a Python module which defines a :term:`WSGI application object` for the
server to call when it gets an incoming HTTP request. (The behavior of this
callable can get a little complex, and is detailed in
:pep:`3333#the-application-framework-side`.) If you already know how to write
WSGI application callables by hand, then maybe you don't always want to
anymore; it's a lot of boilerplate, and duplicated code is lame. If you don't
already know how to write WSGI, you may spend days poring over PEP 3333 to
figure out how to correctly handle all the details. Even if you are careful,
you may spend days to weeks handling corner cases (these can be uncovered more
quickly with a validator like :py:mod:`wsgiref.validate`).

For most people, it would be a colossal waste of time to try to decipher PEP
3333 and handle all the relevant corner cases. Odds are that you will want to
give that a pass and use this or some other tools or frameworks to make WSGI
apps for you. Even if you want to write a framework, you might as well use
existing tools to handle the WSGI part.


Examples
--------

The core functionality of this module is provided by a class named
:class:`WSGI`; instances of this class are run as WSGI apps, and run
some other function containing your code. (In other words, instances of
the class named WSGI are wrappers for your handler functions.)

Instead of writing low-level code for WSGI application callables yourself, you
can typically just write a function which takes a single dict parameter and
returns a string - then wrap it by putting :class:`@WSGI <WSGI>` above the
definition::

    @WSGI
    def handler1(environ):
        return "Hello, world!"

This creates a WSGI application callable which takes two arguments:
(:term:`environ`, :term:`start_response`) and handles the rest. Just configure
your WSGI server to run handler, and it will work. This is the simplest way to
use @WSGI.

If you don't like that @-symbol, you can do the same thing manually::

    def handler2(environ):
        return "Hello, universe!"

    app = WSGI(handler2)

The result of this code is much the same as the previous example, except now
the WSGI callable is named app, and that's what the WSGI server should run (not
handler2, which doesn't itself handle all the WSGI details). 

@WSGI allows you to specify a custom error handler with the 'error_handler'
parameter. This error handler must be a valid WSGI application object
and it should normally do things like logging a traceback. To pass this
parameter, use the form with parentheses::

        @WSGI
        def handler1(environ):
            "WSGI application used as an error handler by handler2."
            return "Oh no!"

        @WSGI(error_handler=handler1)
        def handler2(environ):
            "WSGI application which invokes handler1 when there are errors."
            raise Exception("Let's hear an 'oh no' RIGHT NOW!")

For even more flexibility in how you define your handlers (including the
ability to write handlers which automatically get instances of your favorite
Request class, convert and validate inputs, or return other kinds of values)
check out the tools in the :mod:`transform` module.


Reference
---------
.. autoclass:: wsgi.WSGI
.. autoclass:: wsgi.HandlerError
.. autoclass:: wsgi.IterableWrapper(iterable, environ, start_response, error_handler=default_error_handler)
.. autofunction:: wsgi.default_error_handler
.. autoclass:: wsgi.Middleware
    :members: environ_hook, headers_hook, iterable_hook
.. autoclass:: wsgi.StartResponse
    :members: reset, data
.. autoclass:: wsgi.BadMiddleware
