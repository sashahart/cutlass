.. _storage:

Storage
=======

As a loosely-coupled toolkit focused on general web issues like WSGI and HTTP,
Cutlass doesn't do anything specific with server-side storage. This is by
design. Cutlass is oriented toward flexibility and separation of concerns, and
is specifically meant NOT to dictate how you deal with storage.

Big frameworks like Django and Rails, in their typical deployments, are
essentially generic database applications - they require deployed databases and
associated configuration and significant internal coupling, in order to provide
a lot of powerful features that use a lot of server-side state, and make things
simpler for people with less knowledge of databases.

Trying to use Cutlass to set up the same kind of database-driven CRUD (Create,
Retrieve, Update, Delete) app means finding your own way to interact with
storage, which requires you to make more decisions and do more setup. 

If you are good with a framework (or have detailed documentation for what you
are doing) and can see that your task will fit nicely inside what the framework
does, you should definitely use your framework for that task. In this case,
Cutlass is better for whatever doesn't fit inside the framework, or things
which need to be written at a low level, or integrating across WSGI apps.
(Cutlass was never meant to take over the world, just to provide tools which do
a job and get out of the way.)

If you are using a different ORM or a different kind of datastore, or
a sophisticated combination of datastores, or if you want better control over
the performance of your app, then you are going to have to do significant work
on that layer of your megaframework in any case. In that case, microframeworks
and libraries like Cutlass will often become more useful for the core of a CRUD
app, and it becomes more appropriate to use a standalone storage API designed
by specialists, rather than a more tightly-coupled one included with your web
framework for convenience.

Trying to make a generic interface that abstracts over all possible the
back-end differences is a gargantuan task, and the result tends to be
satisfying in inverse proportion to the dissimilarities among the systems it
abstracts. Even if it were part of the intended purpose for Cutlass, tremendous
resources are required to do a good job. What's the point when SQLAlchemy etc.
already exist?

What should you do? Pick something reasonable and use it. If you don't know
what to use, and particularly if you want to know your business, you cannot
avoid learning about the available solutions.

If you want a generic storage layer with switchable back ends that offers
a Python API, there are a number of choices, e.g.:

- `SQLAlchemy <http://www.sqlalchemy.org/>`_
- `peewee <http://peewee.readthedocs.org/en/latest/>`_
- `Storm <https://storm.canonical.com/>`_
- `SQLObject <http://www.sqlobject.org/>`_
- It is also possible to use the ORM components of some frameworks like Django
  or Web2py as standalones, if you otherwise want to integrate with those
  frameworks.

Python APIs for talking directly to conventional relational databases (e.g. if
you are allergic to ORMs and don't want to use their facilities for integrating
raw SQL):

- `PostgreSQL <http://www.postgresql.org/>`_ with `psycopg <http://initd.org/psycopg/>`_
  (as often used with Django) or alternatively `py-postgresql <http://python.projects.postgresql.org/>`_
- `MySQL <http://www.mysql.com/>`_ with e.g. `PyMySQL <https://github.com/petehunt/PyMySQL>`_
- `Oracle <http://www.oracle.com/index.html>`_ with the `cx-oracle <http://cx-oracle.sourceforge.net/>`_ DB API 2.0 driver

A few other servers you can run yourself, with various specialties, which offer Python APIs:

- `MongoDB <http://www.mongodb.org/>`_ with `PyMongo <http://api.mongodb.org/python/current/>`_
- `Cassandra <http://cassandra.apache.org/>`_ with `pycassa <https://github.com/pycassa/pycassa>`_
- `Redis <http://redis.io/>`_ with `redis-py <https://github.com/andymccurdy/redis-py>`_
- `Kyoto Cabinet <http://fallabs.com/kyotocabinet/>`_
- `Riak <http://wiki.basho.com/Riak.html>`_ with `riak-python-client <https://github.com/basho/riak-python-client>`_
- `Neo4j <http://neo4j.org/>`_ with `neo4j-embedded <http://docs.neo4j.org/chunked/snapshot/python-embedded-installation.html>`_
- `CouchDB <http://couchdb.apache.org/>`_ with `couchdbkit <http://couchdbkit.org/>`_
- `ZODB <http://www.zodb.org/>`_
- `memcached <http://memcached.org/>`_ with `pylibmc <http://sendapatch.se/projects/pylibmc/>`_

Many conventional web hosts offer shared SQL databases. You can also consider
these hosted storage services, depending on your requirements:

- `Google App Engine <http://code.google.com/appengine/>`_ or maybe `AppScale <http://appscale.cs.ucsb.edu/>`_.
- `Amazon SimpleDB <http://aws.amazon.com/simpledb/>`_
  or `Amazon DynamoDB <http://aws.amazon.com/dynamodb/>`_
  or `Amazon RDS <http://aws.amazon.com/rds/>`_.
- `Rackspace Cloud Databases <http://www.rackspace.com/cloud/cloud_hosting_products/databases/>`_
- `EnterpriseDB Postgres Plus <http://www.enterprisedb.com/cloud-database>`_
- `Heroku Postgres <https://postgres.heroku.com/>`_
- `Cloudant <https://cloudant.com/>`_
