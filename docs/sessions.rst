.. _sessions:

Sessions
========

Cutlass doesn't provide sessions to avoid dictating the configuration and use
of :ref:`server-side storage <storage>` or signed cookies. Requirements and
tastes on these issues differ widely, and doing this kind of work at every
request can have a huge impact on performance and deployment complexity -
particularly if you need a framework to handle this for you. If you know
what you are doing and aren't already using a big framework, it's probably
not a big issue that this hasn't been done for you already, because you
are going to be handling storage in your own way anyhow.

If you really do need sessions for some reason, and strictly client-side or
server-side solutions won't work for you, then you can use other components
such as `Beaker <http://beaker.groovie.org/>`_. Beaker is framework-agnostic
and supports multiple storage backends out of the box (e.g. files, memcached,
SQLAlchemy).
