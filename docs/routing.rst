cutlass.routing
===============
.. automodule:: routing

This module provides standalone tools for routing requests in Python based on
the HTTP request method and path.

It doesn't help you handle WSGI details, which is for something like
:py:class:`@WSGI <wsgi.WSGI>` or a framework. It also doesn't help you with
error handling; that can be done with the error_handler argument to @WSGI, or
some exception-catching WSGI middleware.

Examples
--------

   To start routing requests, you'll probably first want to create
   a :py:class:`~routing.Dispatcher` instance::

        from cutlass import Dispatcher

        dispatcher = Dispatcher(
            ('GET', '/', root),
            ('POST', '/hello', hello),
            ('GET', '/people/{person}', people),
        )

   Alternatively, you can add batches of mappings to an existing dispatcher
   with its :py:meth:`~Dispatcher.add` method::

        dispatcher = Dispatcher()
        dispatcher.add([
            ('GET', '/', root),
            ('POST', '/hello', hello),
            ('GET', '/people/{person}', people),
        ])

   If you prefer to put the mappings together with your handler declarations,
   you can use a :term:`decorator`::

        app = Dispatcher()

        @app.GET('/hello')
        def hello(environ, start_response):
            start_response("200 OK", [])
            return [b"too lazy to write real content"]

   you can also do that for any set of methods with @dispatcher.route::

        @app.route(['GET', 'HEAD'], '/')
        def root(environ, start_response):
            start_response("200 OK", [])
            if environ.get('REQUEST_METHOD') == 'HEAD':
                return [b'']
            return [b"too lazy to write a real index page"]

   In all of these cases, the result is the same.

   It would be normal to pass a dispatcher to your server as the app (meaning
   that the server calls the dispatcher as a WSGI app, which then finds and
   runs another WSGI app).
   If you want to manually dispatch in your own code, just call the dispatcher
   like any WSGI app::

        dispatcher(environ, start_response)

   In either case, the dispatcher will look at the request to decide which
   handler to send it to. If it does find a handler, it runs it. If it doesn't
   find anything that matches, it returns a simple default 404 response. To
   customize what happens in this case, pass a WSGI app as the 'default'
   parameter to Dispatcher::

       def my404(environ, start_response):
           start_response("404 Missing", [])
           return ["Put cool site branding and/or suggestion nonsense here"]

       dispatcher = Dispatcher(default=my404)

   Dispatcher and :py:class:`~routing.Mapper` intentionally provide no
   guarantee that mappings will be checked in the order they are defined; this
   is to allow any kind of mapping algorithm to be implemented on the back end
   (including whatever pattern language, rule object, predicates, traversal,
   etc. you want); in particular, to allow dispatch to be implemented as one
   lookup, rather than an interface which REQUIRES a long series of rule
   matches that may interact in unpredictable ways (you could call this "flat
   dispatch", in contrast with what might be called "deep dispatch" approaches,
   like :term:`path traversal`).

   Cascade is useful if you need an app object to be run and then say 'I give up,
   keep looking in a different set of routes.' Have the app return a 404 and
   put it first in the Cascade. This adds a "deep dispatch" capability which
   can be used in just the cases it is required.

.. warning::
   This does not apply to the default FriendlyMapper behavior, but if if you
   use an order-sensitive mapper like RegexMapper, you must be careful about
   using the decorator to add handlers from different modules to the same
   dispatcher, because the order in which they are added will depend on the
   order in which those modules are imported, which can be somewhat
   unpredictable. 


Reference
---------

.. autoclass:: routing.Dispatcher(mappings=Unset, mapper=Unset, default=Unset, ignore_trailing_slash=True)
.. autoclass:: routing.FriendlyMapper(mappings=Unset, ignore_trailing_slash=True)
.. autoclass:: routing.RegexMapper(mappings=Unset, ignore_trailing_slash=True)
.. autoclass:: routing.Mapper
.. autoclass:: routing.Cascade
