# -*- coding: utf-8 -*-
# Cutlass documentation build configuration file.
# This file is execfile()d with the current directory set to its
# containing dir.

import sys, os

# Set path so autodoc can find modules by name.
sys.path.insert(0, os.path.abspath(os.path.join('..', 'cutlass')))

extensions = ['sphinx.ext.autodoc',
              'sphinx.ext.doctest',
              'sphinx.ext.todo',
              'sphinx.ext.coverage',
              'sphinx.ext.intersphinx',
              'sphinx.ext.graphviz']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix of source filenames.
source_suffix = '.rst'

# Ignore these patterns when looking for source files.
exclude_patterns = ['_build', '_bak', 'design']

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'Cutlass'
copyright = u'2011-2012, Sasha Hart'

# TODO: pull this from somewhere else.
# The short X.Y version.
version = '0.0.0'
# The full version, including alpha/beta/rc tags.
release = '0.0.0'

# If true, '()' will be appended to :func: etc. cross-reference text.
add_function_parentheses = False

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
add_module_names = False

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# A list of ignored prefixes for module index sorting.
#modindex_common_prefix = []


# -- Options for HTML output ---------------------------------------------------

from colorsys import rgb_to_hsv, hsv_to_rgb
def h2rgb(h):
    h = h.lstrip("#")
    r, g, b = h[0:2], h[2:4], h[4:6]
    return [int(item, base=16)/255.0 for item in (r,g,b)]
def cin(name):
    color = html_theme_options[name]
    rgb = h2rgb(color)
    hsv = rgb_to_hsv(*rgb)
    print color, rgb, hsv
    return list(hsv)
def rgb2h(r, g, b):
    return "#" + "".join([
        "%.2x" % (item*255.0) for item in (r, g, b)
        ])
def cout(h, s, v, name=""):
    rgb = hsv_to_rgb(h, s, v)
    color = rgb2h(*rgb)
    print h, s, v, rgb, color
    html_theme_options[name] = color

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'cutlass'
html_theme_path = ['.']
#html_style = None  # RTD workaround

html_theme_options = dict(
        # relbarbgcolor='#ff0000',
        # sidebarbgcolor='#00ff00',
        # headbgcolor='#0000ff',
        # codebgcolor='#ffff00',
        # footerbgcolor='#0088ff',
        )

html_show_sourcelink = False
# html_sidebars = {
#         '**': ['localtoc.html', 'searchbox.html']
#         }

def whatever():

    def process(*names):
        hsvs = [cin(name) for name in names if name]
        h = sum([hsv[0] for hsv in hsvs]) / len(hsvs)
        #h = h + .05
        for i in range(0, len(names)):
            hsv = hsvs[i]
            hsv[0] = h
            hsv[1] = hsv[1]/1.5
            name = names[i]
            cout(*hsv, name=name)

    process('relbarbgcolor', 'sidebarbgcolor', 'footerbgcolor')

    # Theme options are theme-specific and customize the look and feel of a theme
    # further.  For a list of options available for each theme, see the
    # documentation.
    #html_theme_options = {}

# Add any paths that contain custom themes here, relative to this directory.
#html_theme_path = []

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
#html_last_updated_fmt = '%b %d, %Y'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
#html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
#html_sidebars = {}

# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
#html_domain_indices = True

# If false, no index is generated.
#html_use_index = True

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, links to the reST sources are added to the pages.
#html_show_sourcelink = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#html_show_sphinx = True

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = None

# Output file base name for HTML help builder.
htmlhelp_basename = 'Cutlassdoc'


# -- Options for LaTeX output --------------------------------------------------

# The paper size ('letter' or 'a4').
#latex_paper_size = 'letter'

# The font size ('10pt', '11pt' or '12pt').
#latex_font_size = '10pt'

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title, author, documentclass [howto/manual]).
latex_documents = [
  ('index', 'Cutlass.tex', u'Cutlass Documentation', u'Sasha Hart', 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = False

# If true, show page references after internal links.
#latex_show_pagerefs = False

# If true, show URL addresses after external links.
#latex_show_urls = False

# Additional stuff for the LaTeX preamble.
#latex_preamble = ''

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_domain_indices = True


# -- Options for manual page output --------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'cutlass', u'Cutlass Documentation', [u'Sasha Hart'], 1)
]

intersphinx_mapping = {
    'python2': ('http://docs.python.org/2.7', None),
    'python3': ('http://docs.python.org/3.3', None),
    }
intersphinx_cache_limit = 90
