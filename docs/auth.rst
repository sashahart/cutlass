.. _auth:

Auth
====

Cutlass doesn't prescribe how you will secure any part of your website.
Here is a list of some options.

- `Persona <www.mozilla.org/persona/>`_ (formerly BrowserID) is an easy option
  which doesn't require your to handle passwords, only include a Javascript
  library and supply a server-side handler which checks a received string
  against a verifier service (which you can also run yourself, if you want to)
- `Janrain <http://www.janrain.com/>`_ (formerly rpxnow) is a hosted commercial
  service which lets users log in from many different social networks; as of
  this writing they have a free basic tier up to a certain number of users per
  year.
- `repoze.who <http://docs.repoze.org/who/2.0/>`_  does authentication/login
  either as middleware or as an explicit API, and at this writing seems to be
  the last WSGI login middleware still standing, with a number of addons.
- `velruse <http://packages.python.org/velruse/>`_ is a set of open-source auth
  routines which can also be run on your hardware as a self-hosted service
  (based on Pyramid).
- If you are using Apache, `mod_wsgi offers its own mechanisms for access control
  <http://code.google.com/p/modwsgi/wiki/AccessControlMechanisms>`_
  (though of course this will not be portable between servers).
