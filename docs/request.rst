cutlass.request
===============
.. automodule:: request

cutlass.request contains tools for dealing with HTTP/WSGI requests in handlers.
This module is completely optional and standalone, without anything else in
Cutlass depending on it. Use whatever request parsing code you want, without
penalty. This one is meant to provide a simple, relaxed interface to reduce
boilerplate, and easily pass request data around among components.

Why?
----

A WSGI server passes HTTP request data to apps in the form of an environ dict.
This is pretty usable on its own, but directly reading some kinds of request
information out of an environ dict (e.g. forms, file uploads, cookies) can be
a drag associated with a lot of extra code and corner cases, so it's a much
better idea to automate it with some tool. The Python standard library only
goes so far in helping with these, so almost every WSGI project or framework
defines or uses its own set of wrappers and utility functions for request
parsing. 

Despite various differences, these tools all function very similarly. There are
probably no great technical advances to be made in Request objects, so you'd
usually choose what to use based on compatibility or taste.

The Cutlass Request class isn't required by anything else. So its entire
purpose is to cater to a particular taste: a simple, relaxed interface which
reduces boilerplate but doesn't introduce a lot of automatic behavior or
mandatory overhead. It's also designed to allow information on a WSGI request
to be passed around among components, and to act like a WSGI environ in
a pinch.

Features
--------

Here are some of the distinctive characteristics of the Cutlass Request class.

    - It exposes convenient syntax as in 'request.path_info', and can act like
      an environ dict. Mirroring CGI itself, it ignores the case of keys and
      returns None on a missing key by default, rather than automatically
      breaking program flow with exceptions that you have to write boilerplate
      to catch.
    - It uses standard CGI and WSGI names for variables (as one does with an
      environ), So you don't need to learn new names for everything or change
      gears as you do when switching between a framework and lower-level
      environ access.
    - Modifying it won't modify the passed environ. 
      (If you want that behavior, it's recommended to use WebOb Request.)
    - Modifications to the request object can be rolled back to the original
      state (or selectively reveal that state) in case a consumer needs to see
      it.
    - It is lazy about parsing cookies, forms, and files and only requires the
      associated libraries if these are actually accessed. 

If that's what you want, try this class. If not, it should be no problem to
use any class you prefer.

.. note::
   If you choose to use request.py on its own or remove other files, please
   note that using request.cookies requires 'cookies.py' while request.form and
   request.files require Marcel Hellkamp's 'multipart.py' (as included). Other
   attributes should work as normal even without these files.
      

Examples
--------

Instantiating a request is simple. Here is how it might be used in a bare WSGI callable,
without any other components::

    def app1(environ, start_response):
        request = Request(environ)
        buf = "Hello, %s" % request.remote_addr
        return [buf]

Or, using :mod:`@WSGI<wsgi>`::

    @WSGI
    def app2(environ):
        request = Request(environ)
        return ["Who cares about the request?"]

You can easily abstract out environ from your handlers using
:mod:`@transform<transform>`::

    @WSGI                       # takes (environ, start_response) and passes environ
    @transform(source=Request)  # passes in Request(environ) to app3
    def app3(request):          # app3 can take for granted that it gets a Request
        return ["Hi, %s" % request.remote_addr]

Exactly the same trick works for any Request object which can be instantiated
with a single environ parameter; this happens to include most widely used
Request classes. 

And you can roll this into your own reusable :term:`decorator`, if you are so
inclined::

    def handler(fn):
        # wrap fn with a source=Request transform, then wrap that in a WSGI object.
        return WSGI(transform(source=Request)(fn))

    @handler
    def app3(request):
        return ["Hi, %s" % request.remote_addr]


Reference
---------

.. autoclass:: request.Request
   :members: get, set, peek, reset, update, yarp

To replace all of a Request's mutable cache at once, just change its .data
attribute::

    request.data = {'foo': 'bar'}

.. autoclass:: request.ProtectedError
