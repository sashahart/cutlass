cutlass.response
================
.. automodule:: response

This module contains optional, standalone tools for generating HTTP responses
with little boilerplate. 

Using with @WSGI
----------------
Using this Response class is one among many possible ways of customizing status
and headers inside @WSGI. Anything with the following `WebOb-like attributes
<http://docs.webob.org/en/latest/reference.html#core-attributes>`_ will work:
    * .status (string like "200 OK" as in WSGI), 
    * .headers (any mapping object which flattens with .items(), like a dict or wsgiref.headers.Headers or WebOb ResponseHeaders) 
    * and .body (string) OR .app_iter (iterator yielding strings, for use in situations where the whole thing might be large and you don't want it all at one time).

Anything satisfying this interface will work the same with @WSGI. If you aren't
using @WSGI then there is nothing to couple any part of Cutlass to the way you
generate responses.

Other use cases
---------------

If you are writing raw-WSGI handlers, you can make an instance of Response and
then call instance.send(environ, start_response) to send it simply. If you just
want the lower level imperative API, you can use functions like
render_response() yourself or in your own wrappers. 

Otherwise, if you are not using @WSGI, you can use anything you want.

Reference
---------

.. autoclass:: response.Response

   .. attribute:: status

      The versionless HTTP Status-Line for the response, such as '200 OK'. 
      :attr:`status_code <Response.status_code>` 
      and :attr:`reason_phrase <Response.reason_phrase>` 
      can be used to change or access the individual parts of the
      status, and are updated when status is set directly.

   .. attribute:: status_code

      The Status-Code part of the HTTP status - an integer, such as 200. If
      this is set directly (i.e., not through status) then a generic
      reason_phrase is chosen to complement it.

   .. attribute:: reason_phrase

      The Reason-Phrase part of the HTTP status - a string, such as 'OK'. This
      should be a brief description of the status for humans to read.

   .. attribute:: body

      The complete body for the response, for cases when the whole thing should
      be retrieved in one piece.
      
      If :attr:`app_iter <Response.app_iter>` is defined on the response, it
      will be used instead of :attr:`body <Response.body>` - collapsing it (and
      possibly consuming the :term:`iterable <response iterable>`) in the
      process. If you set body, app_iter is cleared, so that the new body will
      be given instead of the collapsed app_iter.

   .. attribute:: app_iter
      
      The iterable version of the response body, for use when the whole thing
      should NOT be handled at once. If set, then accessing :attr:`body
      <Response.body>` will actually access a collapsed version of app_iter.
      
      If you need to test whether this is set, use ``if response.app_iter`` or
      ``if not response.app_iter``.

   .. attribute:: headers

      A dict-like object, mapping names of HTTP response headers such as
      'Content-Type' to header values such as 'text/plain'. 
      When the response is rendered into WSGI format, these headers are
      flattened into a list of tuples by calling headers.items().

      Changes to headers may be reflected in other attributes, such as 
      :attr:`content_type <Response.content_type>`, to keep them in sync.
  
   .. attribute:: content_type
      
      The complete value for the Content-Type header, including the mime_type,
      charset, and content_type_params components.

   .. attribute:: mime_type

      Represent only the MIME type component of the Content-Type header value.

   .. attribute:: charset

      Represent only the charset component of the Content-Type header value.

   .. attribute:: content_type_params

      Represent only the parameters component of the Content-Type header value.

.. autoclass:: response.Error
.. autoclass:: response.NotFound
.. autoclass:: response.Redirect
.. autoclass:: response.Forbidden
