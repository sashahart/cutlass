.. _about_the_name:

About the name 'cutlass'
------------------------

The name of the project reflects the aspirations of its design. 

A cutlass is a tool, like a machete, essential to farming in many parts of the
world. It is widely available, relatively inexpensive, and extremely simple. It
requires little in the way of maintenance or training. It has no notable
'features,' but that simplicity gives it a thousand practical uses. It does
what you tell it. It's humble, and humility makes it powerful.

Cutlass isn't meant to take over your development process, let alone the world.
It's meant to be a humble tool that works with others. It doesn't need to do
everything, and it shouldn't constrain what you do with it. It should just
facilitate essential tasks, with a minimum of fuss and complexity.
