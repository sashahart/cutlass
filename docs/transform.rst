cutlass.transform
=================
.. automodule:: transform

This might be easier to demonstrate than to explain, so here's a simple
illustration of what it does::

    >>> from cutlass import transform
    >>> def int_args(*items):
    ...     "coerce all arguments to int"
    ...     # return args, kwargs for next function
    ...     return [int(item) for item in items], {}
    ... 
    >>> @transform(source=int_args, target=str)
    ... def add(x, y):
    ...     return x + y
    ... 
    >>> result = add("7", "8")
    >>> result
    '15'
    >>> type(result)
    <type 'str'>

Write a WSGI handler in terms of a webob request and a JSON response::

    from cutlass import WSGI
    from webob.request import Request

    @WSGI
    @transform(source=Request, target=json.dumps)
    def handler(request):
        return {'addr': request.remote_addr}


Reference
---------

.. autofunction:: transform.transform
.. autofunction:: transform.transformed
