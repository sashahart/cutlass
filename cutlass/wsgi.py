"""Wrap callables to convert them into WSGI application objects that do error
handling, call start_response, interpret and encode the return value, etc.
"""

import sys
import logging
import traceback
try:
    callable
except NameError:
    callable = lambda f: hasattr(f, "__call__")

# Errors which make the life of a process not worth living any more
_FATAL_EXCEPTIONS = (MemoryError, IOError, SystemError, KeyboardInterrupt)


class HandlerError(Exception):
    """Errors generated by and specific to cutlass.wsgi.
    """
    def __init__(self, message=''):
        """
        :arg message: internal error message for logging.
        """
        Exception.__init__(self)
        self.message = message

    def __str__(self):
        if self.message:
            return Exception.__str__(self) + self.message
        return Exception.__str__(self)


def default_error_handler(environ, start_response):
    """Bare-bones fallback WSGI app for exception handling.

    Logs a traceback, calls start_response to give the server a chance to abort
    with an error, and otherwise return a blank page with status 500.
    """
    # Check for an exc_info - if there isn't one, traceback will fail
    exc_info = sys.exc_info()
    if exc_info != (None, None, None):
        # dumbly log the traceback to wherever the logging module is pointed.
        logging.error("Handler error:\n %s", traceback.format_exc())

    # Pass exc_info, for the sake of paranoia: we don't know if something else
    # might have already called start_response, or if header data was already
    # sent. If so, server should have a chance to halt.
    start_response("500 Internal Server Error", [], exc_info=exc_info)

    exc_info = None
    # Raw WSGI dictates bytestrings
    return [b""]


def default_runner(handler, environ, start_response):
    """Wrap handler calls to deliver WSGI app object behavior.

    This runner simply passes an environ, calls unpack_response on its return
    value and passes the unpacked header data to start_response to fulfill WSGI
    requirements.

    This intentionally does not do error handling, which is an independent
    concern. Feel free to write an alternative to default_runner to implement a
    new handler invocation protocol, and then run your new runner with
    run_runner to reuse the error handling behavior. If you don't need the
    error handling behavior, you can just roll your own solution entirely.

    :arg handler:
        the handler to be run. For this runner, a handler is a callable that
        can run with just an environ parameter and returns a string, an
        iterable of strings, or an object with status/headers/body attributes.

    :arg environ:
        environ to pass through to the handler.

    :arg start_response:
        start_response callable (e.g. from the server) for default_runner to
        use to buffer headers for sending.
    """
    result = handler(environ)
    status, headers, iterable = unpack_response(result)
    # Strings are iterable, but not at all the right thing to have here...
    assert not isinstance(iterable, type('')), (
            "handler returned a string instead of a response iterable")
    start_response(status, list(headers))
    return iterable


def run_runner(runner, handler, environ, start_response,
               methods=None, error_handler=None, iterable_wrapper=None):
    """Use an arbitrary function to run a handler, but try to call a WSGI app
    to handle any errors.

    This is meant to allow different functions to be used to run handlers,
    implementing different conventions about how they are called, how their
    responses are interpreted and how other business is handled. Each runner
    deals with a specific convention or protocol for running handlers; this
    function just does error handling around any specific runner.

    Most people wouldn't need to mess with this stuff, just use @WSGI if you
    want a simpler way to define WSGI application objects. And other people
    will want to implement their own all-in-one runner to do handler
    invocation, error handling etc. - those people can just ignore this module
    and roll their own.

    :arg runner:
        A callable which will be invoked to run the handler.

    :arg handler:
        The handler callable, to be run by handler_runner.

    :arg environ:
        WSGI environ for this request; the runner should make this some form of
        this data available to the handler somehow.

    :arg start_response:
        WSGI start_response for this request; the runner should either call
        start_response itself and not pass it to the handler, or pass it to
        the handler and not call it.

    :arg error_handler:
        If None, all errors will be reraised.
        A WSGI application object to deal with exceptions during handler run
        (e.g. logging a traceback).
        As a WSGI app object, and to satisfy WSGI requirements for error
        handling, this must take (environ, start_response), call start_response
        with the result of sys.exc_info(), and return an iterable of ISO 8859-1
        str.
        This is hardcoded to WSGI convention since this is the WSGI module and
        is used in the context of WSGI-style error handling.
        if you don't want to write raw WSGI error handlers, use tools like
        @WSGI to simplify the task.

    :arg iterable_wrapper:
        Callable used to wrap the iterable returned by the handler or the
        error_handler. If None, it won't be wrapped.

    :returns:
        An iterator wrapped by iterable_wrapper, or just the original iterator
        if there was no iterable_wrapper.
    """
    # support optional method sanity check, don't even call handler if failed
    if methods and not environ.get('REQUEST_METHOD') in methods:
        start_response("405 Method Not Allowed",
                       [('Allow', ",".join(methods))])
        return ['']
    if not callable(runner):
        raise HandlerError("runner is not callable")
    if not callable(handler):
        raise HandlerError("handler is not callable")

    # PEP 3333: 'In general, applications should try to trap their own,
    # internal errors' - and example code has app catching most possible
    # exceptions to call start_response with an exc_info argument;
    # when in Rome...
    try:
        iterable = runner(handler, environ, start_response)
    # Reraise obvious heavy-duty errors which mean it's stupid to continue, but
    # error_handler should get a shot at everything else.
    except _FATAL_EXCEPTIONS:
        raise
    except:
        if not error_handler:
            raise
        # Call a WSGI app to handle the error (logging, error page, etc.) If it
        # fails, call the dead simple default handler, which is hard coded on
        # purpose. If that fails (!) then the error should be left alone to
        # propagate to the server.
        if error_handler is default_error_handler:
            iterable = error_handler(environ, start_response)
        else:
            try:
                iterable = error_handler(environ, start_response)
            except _FATAL_EXCEPTIONS:
                raise
            except:
                logging.error("error_handler failed, "
                              "trying default_error_handler")
                iterable = default_error_handler(environ, start_response)
        # Don't wrap error iterable.
        return iterable

    # If there is an iterable wrapper, use it - this lets us do things like
    # catch errors during iteration, so they can be handled consistently with
    # errors during app object execution.
    if not iterable_wrapper:
        return iterable
    return iterable_wrapper(iterable, environ, start_response, error_handler)


class WSGI(object):
    """Decorator for defining WSGI app objects in terms of simpler handler
    functions which take an environ and return a string, iterable or response
    object.

    This does the following:
        - Handles errors per WSGI, in a customizable way
        - Deals with start_response for you (and keeps it out of the handler)
        - Lets handlers pass back their results in easier ways than raw WSGI
        - Coerces body iterables to ISO 8859-1 str as necessary

    If you want to generate WSGI app objects easily without thinking much about
    WSGI, this is what you want. Put @WSGI before a function which takes an
    'environ' parameter and you are done. You can also invoke it as @WSGI() if
    you need to pass parameters. It's also possible to directly instantiate
    the WSGI class and configure it imperatively.

    You can also subclass this to create a new decorator, or wrap callable
    object instances which don't store request data; but if you do those
    things, then take care not to leak request data across requests.

    :arg error_handler:
        WSGI application object to use to handle errors and generate error
        responses (defaults to the minimal default_error_handler if
        unspecified).

    :arg iterable_wrapper:
        An iterator used to wrap whatever the handler returns (e.g. to handle
        errors or encoding). Defaults to DefaultIterableWrapper. If None, the
        iterable is not wrapped at all.

    :arg methods:
        A whitelist of accepted HTTP methods. If the request's method is
        not on the whitelist, the decorated handler will not be run.
        Instead, a simple 405 will be generated. This should not normally
        happen because usually, a server/router will be checking methods
        up front. This is a safeguard.
        Defaults to None, which means no method checking is done here.
    """
    def __init__(self, *args, **kwargs):
        # If args[0] is callable, __call__ might need it pre-stored
        # Otherwise, we have to wait until __call__ to get a handler
        if args and callable(args[0]):
            self.handler = args[0]
        else:
            self.handler = None

        if kwargs:
            self.runner = kwargs.get('runner', default_runner)
            self.error_handler = kwargs.get('error_handler',
                                            default_error_handler)
            self.iterable_wrapper = kwargs.get('iterable_wrapper',
                                               DefaultIterableWrapper)
            self.methods = kwargs.get('methods', None)
        else:
            self.runner = default_runner
            self.error_handler = default_error_handler
            self.iterable_wrapper = DefaultIterableWrapper
            self.methods = None

    def __call__(self, handler_or_environ, start_response=None, **kwargs):
        # Here we infer whether the decorator call was @decorator or
        # @decorator(2) style. if no args or args[0] not callable, this is not
        # a parameterized decorator invocation - thus, this method does not
        # decorate anything, and may act as if it were the decorated handler
        if not handler_or_environ or not callable(handler_or_environ):
            if start_response is None:
                raise HandlerError(
                        "__call__ got no arg[1] - should be start_response")
            return self.call(handler_or_environ, start_response)
        else:
            # If we are here, then the current args[0] is a callable, so we
            # should decorate unless __init__ got a callable args[0] (in which
            # case self.handler is None) - in that latter case, the situation
            # is too confused, and we should quit.
            if self.handler is not None:
                raise HandlerError(
                    "decorator is confused since __init__ and __call__"
                    " both got callable args[0]")
            # Store the handler received and return a decorated function which
            # will call it.
            self.handler = handler_or_environ
            return self.call

    def call(self, environ, start_response):
        """Call the wrapped handler inside a runner.

        This is invoked by __call__, which is complex enough (for the sake of
        providing flexible decorator syntax) that you probably don't want to
        mess with __call__. The standard way to change the behavior of this
        class is to specify a different runner, but you can also override
        this method if you rneeds are more radical. If you don't like the
        decorator interface either then you should just roll your own.

        The core functionality is left in run_runner so that the underlying
        behavior can be reused in other code.
        """
        result = run_runner(self.runner, self.handler, environ, start_response,
                            methods=self.methods,
                            error_handler=self.error_handler,
                            iterable_wrapper=self.iterable_wrapper)
        return result


def unpack_response(result):
    """Interpret a handler's return value as an implied HTTP response.

    Used by default_runner, and exposed separately in the event that the same
    behavior is needed anywhere else.
    """
    status = "200 OK"
    headers = []
    iterable = []
    if result is None:
        raise HandlerError("handler did not return a value")
    # 1. result is itself a string body (native string, that is)
    # 2. result is object with status, headers, app_iter attribs
    # 3. result is itself an iterable body
    if isinstance(result, str):
        return status, headers, [result]
    try:
        status = result.status
        # n.b.: per WSGI, headers must be of type list. but that is not
        # enforced here, in case someone wants to use a dict_items view
        # in a runner; this means it must be handled downstream.
        headers = result.headers.items()
        iterable = getattr(result, 'app_iter', None)
        if not iterable:
            iterable = [result.body]
    except (AttributeError, TypeError):
        try:
            iter(result)
            return status, headers, result
        except TypeError:
            raise HandlerError(
                    "value returned by handler has unrecognized type: %r"
                    % type(result))
    return status, headers, iterable


class IterableWrapper(object):
    """Generic interface/code for making WSGI-iterable wrappers.
    """
    def __init__(self, iterable, error_handler=None):
        self.iterable = iterable
        self.error_handler = error_handler

    def close(self):
        """Allow the wrapped iterable to be closed by the WSGI server.

        This can also be overridden to customize teardown logic.
        """
        if hasattr(self.iterable, "close"):
            return self.iterable.close()

    def __iter__(self):
        """Implement the iterator interface, with error-handling boilerplate.

        Unless your needs are subtle, you probably want to override wrap().
        """

        def encode(item):
            # better be string-like!
            assert hasattr(item, 'encode')
            return item.encode('ISO-8859-1')

        try:
            wrapper = self.wrap(self.iterable)
            for item in wrapper:
                yield item
        except _FATAL_EXCEPTIONS + (StopIteration, GeneratorExit,):
            raise
        except:
            error_handler = self.error_handler
            # If user did not give error handler, let it pass by default
            if not error_handler:
                raise
            # Use provided error_handler to generate the response. It's
            # reasonably likely that error_handler, by calling
            # start_response with exc_info, will cause the server to
            # raise an exception (e.g., because some data was already
            # sent by the server). Otherwise, infer we can send data.
            try:
                iterable = error_handler(self.environ, self.start_response)
                for item in iterable:
                    yield item
            except _FATAL_EXCEPTIONS:
                raise
            # If user's error handler fails, use default to report
            except:
                # default_error_handler definitely must pass exc_info!
                iterable = default_error_handler(self.environ,
                        self.start_response)
                for item in iterable:
                    yield item
        raise StopIteration

    def wrap(self, iterable):
        """Override this method with a generator based on iterable.

        If overriding does not suit, just set .wrap to a non-method
        which does the same thing.

        It's run inside an error handler in __init__ - this is intended
        to make things easier than reproducing that kind of boilerplate
        every time.
        """
        iterable = iterable or self.iterable
        for chunk in iterable:
            yield chunk


class DefaultIterableWrapper(IterableWrapper):
    """Wrap a WSGI body iterable to force ISO 8859-1 str and handle errors.

    This is the wrapper used by @WSGI by default.
    """
    def __init__(self, iterable, environ, start_response,
                 error_handler=default_error_handler):
        IterableWrapper.__init__(self, iterable=iterable,
                error_handler=error_handler)
        self.environ = environ
        self.start_response = start_response
        self.error_handler = error_handler
        # Track how many times we've yielded, for debugging
        self.yield_count = 0

    def wrap(self, iterable):
        encode = lambda item: item.encode('ISO-8859-1')
        self.latest_chunk = None
        for chunk in iterable:
            self.latest_chunk = chunk
            if not isinstance(chunk, bytes):
                chunk = encode(chunk)
            yield chunk
            self.yield_count += 1

    def error_handler(self, environ, start_response):
        # Stuff some debug info in, for lack of a better channel;
        # an error handler which uses this is at least no worse
        # than using a proprietary signature for error handlers
        environ['iterable_yield_count'] = self.yield_count
        environ['iterable_latest_chunk'] = self.latest_chunk
        # Just do the normal thing
        return default_error_handler(environ, start_response)


class BadMiddleware(Exception):
    """Raised when a Middleware subclass does something wrong.
    """
    def __init__(self, instance, message):
        self.cls = instance.__class__
        self.message = message

    def __str__(self):
        return "%r: %s" % (self.cls, self.message)


class StartResponse(object):
    """Simple dummy start_response used to collect headers for processing.
    """
    def __init__(self):
        self.reset()

    def reset(self):
        """Reset state so that object is ready for a new call.
        """
        self.called = False
        self.status = ""
        self.headers = []
        self.exc_info = None

    def __call__(self, status, headers, exc_info=None):
        """Collect information from WSGI app which thinks we are the server.
        """
        self.called = True
        self.status = status
        self.headers = headers
        self.exc_info = exc_info

    def data(self):
        """Return data as one unpackable tuple.
        """
        return self.status, self.headers, self.exc_info


class Middleware(object):
    """Class-based template/shortcut for making WSGI middleware.

    To make your own middleware, subclass this and override change_environ,
    change_headers, and/or change_iterable. To use your middleware, make an
    instance of it that wraps a WSGI app.
    """
    def __init__(self, app):
        self.app = app

    def environ_hook(self, environ):
        """Override this to transform the environ going into the app.

        Whatever is returned is what the app will get. If you change the
        environ in-place, be sure to return it to ensure the right thing
        happens.
        """
        # defaults to noop, override for interesting behavior
        return environ

    def headers_hook(self, status, headers, exc_info=None):
        """Override this to transform the app's status, headers, exc_info.

        Whatever is returned is what will be used instead of what the app
        generated.
        """
        # defaults to noop, override for interesting behavior
        return status, headers, exc_info

    def iterable_hook(self, iterable):
        """Override this to replace the iterable returned by the app.
        """
        # defaults to noop, override for interesting behavior
        return iterable

    def __call__(self, real_environ, real_start_response):
        """Run the wrapped object, calling middleware hooks as appropriate.

        Normally you would not override this.
        """
        # Call the hook to modify or substitute the environment
        environ = self.environ_hook(real_environ)
        if not environ and environ != {}:
            raise BadMiddleware(self, "environ_hook did not return an environ")

        # Give the application a sham start_response to collect the header data
        # it generates, and keep its iterable around so we can wrap it
        start_response = StartResponse()
        iterable = self.app(environ, start_response)
        data = start_response.data()
        new_data = self.headers_hook(*data)
        if not new_data:
            raise BadMiddleware(self, "headers_hook did not return anything")
        status, headers, exc_info = new_data
        # If there was no original exception/exc_info, the hook probably
        # does not mean to return one, and it's a bad idea anyway,
        # because downstream code may use sys. functions
        # and downstream humans may get confused about the traceback
        if not data[2]:
            exc_info = None

        # Send the (possibly changed) header data upstream ASAP. Do not
        # pass exc_info at all unless provided, since the start_response
        # may care about 2 vs. 3 args.
        if exc_info is not None:
            real_start_response(status, headers, exc_info=exc_info)
        else:
            real_start_response(status, headers)

        # Return wrapped or replaced iterable.
        new_iterable = self.iterable_hook(iterable)
        try:
            iter(new_iterable)
        except TypeError:
            raise BadMiddleware(self, "iterable_hook did not return anything")
        return new_iterable
