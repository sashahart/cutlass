try:
    from StringIO import BytesIO
except ImportError:
    from io import BytesIO
from textwrap import dedent
from pytest import raises

from cutlass.request import Request, ProtectedError, read_request_body
from cutlass.cookies import Cookies


class TestRequest(object):

    def setup_method(self, method):
        self.request = Request()

    def test_initial_attribute(self):
        r = Request(dict(foo='bar'))
        assert r['foo'] == 'bar'

    def test_set_attribute(self):
        r = self.request
        r.foo = 3
        assert "foo" in r
        assert r.foo == 3

    def test_translate_key(self):
        r = self.request
        r['flug-zeug'] = 4
        assert r['flug-zeug'] == 4
        assert r.flug_zeug == 4
        r['bip bop'] = 'yam'
        assert r.bip_bop == 'yam'

    def test_private_noset(self):
        r = self.request
        keys = [key for key in dir(r)
                if key.startswith('__')]
        assert '__init__' in keys
        assert '__getattr__' in keys
        assert '__setattr__' in keys
        for key in keys:
            print(key)
            with raises(ProtectedError):
                setattr(r, key, None)
            with raises(ProtectedError):
                r.update({key: None})

    def test_protected_nocontains(self):
        r = self.request
        keys = sorted(r._protected)
        assert '__init__' in keys
        for key in keys:
            assert key not in r

    def test_reset(self):
        r = self.request
        r['foo'] = 3
        assert r.foo == 3
        r.reset()
        assert 'foo' not in r

    def test_initial_not_in_cache(self):
        r = Request(dict(foo='bar'))
        assert 'foo' not in r._cache
        assert r._data == {'foo': 'bar'}

    def test_noninitial_in_cache(self):
        r = Request(dict(foo='bar'))
        r['bam'] = 'baz'
        assert 'bam' not in r._data
        assert r._cache == {'bam': 'baz'}
        assert r.bam == 'baz'
        r.yam = 3
        assert 'yam' in r._cache
        assert 'yam' not in r._data
        r.set('fob', 'flup')
        assert 'fob' in r._cache
        assert 'fob' not in r._data

    def test_peek(self):
        r = Request(dict(foo='bar'))
        assert r.foo == 'bar'
        r.foo = 'baz'
        assert r.foo == 'baz'
        assert r.peek('foo') == 'bar'

    def test_raise_keyerror(self):
        "Raises KeyError when requested, not otherwise."
        r = self.request
        assert r.get('foo') == None
        with raises(KeyError):
            r.get('foo', raise_keyerror=True)

    def test_setdefault(self):
        r = self.request
        assert 'foo' not in r
        assert r.setdefault('foo', 4) == 4
        assert r.foo == 4
        assert r.setdefault('foo', 8) == 4

    def test_case_insensitive(self):
        r = self.request
        r.FOO = 3
        assert r.FOO == 3
        assert r.foo == 3
        r.baz = 4
        assert r.baz == 4
        assert r.BAZ == 4

    def test_data_descriptor(self):
        r = Request(dict(foo='bar'))
        r.baz = 'bam'
        assert 'foo' in r._data
        assert 'baz' in r._cache and 'baz' not in r._data
        assert r.data == {'foo': 'bar', 'baz': 'bam'}

    def test_query_descriptor(self):
        querystring = "foo=bar&baz=bam"
        r = Request(dict(QUERY_STRING=querystring))
        assert r['QUERY_STRING'] == querystring
        assert r.query['foo'] == ['bar']
        assert r.query['baz'] == ['bam']

    def test_cookies_descriptor(self):
        cookiestring = "a=b; c=d; e=f"
        r = Request(dict(HTTP_COOKIE=cookiestring))
        assert r['HTTP_COOKIE'] == cookiestring
        cookies = r.cookies
        assert isinstance(cookies, Cookies)
        assert 'a' in cookies
        assert cookies != Cookies()

    def test_cookies_created_lazily(self):
        r = self.request
        assert r._cookies is None
        r.cookies
        assert r._cookies is not None

    def test_body_descriptor(self):
        body = b"foo bar baz"
        r = Request({
            'wsgi.input': BytesIO(body),
            'CONTENT_LENGTH': len(body),
            'CONTENT_TYPE': 'text',
            })
        assert r.body == body

    def test_form_and_files(self):
        file_data = "... contents of file1.txt ..."
        body = dedent("""
            --AaB03x
            Content-Disposition: form-data; name="noodle"

            Larry
            --AaB03x
            Content-Disposition: form-data; name="foobar"; filename="file1.txt"
            Content-Type: text/plain

            {0}
            --AaB03x--
        """).strip().format(file_data).encode('utf-8')
        r = Request({
            'REQUEST_METHOD': 'POST',
            'CONTENT_TYPE': 'multipart/form-data; boundary=AaB03x',
            'CONTENT_LENGTH': len(body),
            'wsgi.input': BytesIO(body),
        })
        part = r.files['foobar']
        assert part.filename == 'file1.txt'
        stream = part.file
        data = stream.read()
        assert data == file_data.encode('utf-8')
        # TODO: is it OK to users that form values come out encoded and
        # file data don't - that's inconsistent? What should really
        # happen and do we need to mediate from multipart (ugh)?
        assert r.form.get('noodle') == 'Larry'
        # Second time, in case we aren't fetching from cache
        assert r.form.get('noodle') == 'Larry'

    def test_form_urlencoded(self):
        body = b"Name=Jonathan+Doe&Age=23&Formula=a+%2B+b+%3D%3D+13%25%21"
        r = Request({
            'REQUEST_METHOD': 'POST',
            'CONTENT_TYPE': 'application/x-www-form-urlencoded',
            'CONTENT_LENGTH': str(len(body)),
            'wsgi.input': BytesIO(body),
        })
        form = r.form
        print("form=", dict(form))
        assert form['Name'] == 'Jonathan Doe'
        assert form['Age'] == '23'
        assert form['Formula'] == "a + b == 13%!"


class TestReadRequestBody(object):

    def pytest_funcarg__env(request):
        body = b'second'
        stream = BytesIO(body)
        env = {
            'wsgi.input': stream,
            'CONTENT_TYPE': 'text/plain',
            'CONTENT_LENGTH': str(len(body)),
        }
        return env

    def test_invalid_length(self, env):
        "When CONTENT_LENGTH unparseable, return None"
        env['CONTENT_LENGTH'] = 'gerbil'
        assert read_request_body(env, 3) == None

    def test_no_length(self, env):
        "When CONTENT_LENGTH is unset, do not read and return ''"
        del env['CONTENT_LENGTH']
        assert read_request_body(env, 3) == b''
        assert env['wsgi.input'].tell() == 0

    def test_empty_length(self, env):
        "When CONTENT_LENGTH is empty, do not read and return ''"
        env['CONTENT_LENGTH'] = ''
        assert read_request_body(env, 3) == b''
        assert env['wsgi.input'].tell() == 0

    def test_over_limit(self, env):
        "When CONTENT_LENGTH is over limit, don't read, return None"
        env['CONTENT_LENGTH'] = '4'
        assert read_request_body(env, 3, log=True) == None
        assert env['wsgi.input'].tell() == 0

    def test_at_limit(self, env):
        "WHen CONTENT_LENGTH and limit are equal, it works"
        env['CONTENT_LENGTH'] = '6'
        assert read_request_body(env, 6) == b"second"
        assert env['wsgi.input'].tell() == 6

    def test_under_limit(self, env):
        "When limit is over CONTENT_LENGTH, it works"
        env['CONTENT_LENGTH'] = '3'
        assert read_request_body(env, 7) == b"sec"
        assert env['wsgi.input'].tell() == 3

    def test_under_limit_full(self, env):
        "When given limit above CONTENT_LENGTH, go up to CONTENT_LENGTH"
        env['CONTENT_LENGTH'] = '6'
        assert read_request_body(env, 7) == b"second"
        assert env['wsgi.input'].tell() == 6
