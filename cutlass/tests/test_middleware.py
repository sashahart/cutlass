import sys
from cutlass.wsgi import Middleware, StartResponse, BadMiddleware
from pytest import raises

APP = lambda environ, start_response: \
    start_response("200 OK", []) and ['OK']


class TestNoReturns(object):

    def test_environ(self):

        class M(Middleware):
            def environ_hook(self, environ):
                pass

        environ = {}
        sr = StartResponse()
        with raises(BadMiddleware):
            M(APP)(environ, sr)

    def test_headers(self):

        class M(Middleware):
            def headers_hook(self, status, headers, exc_info=None):
                pass

        environ = {}
        sr = StartResponse()
        with raises(BadMiddleware):
            M(APP)(environ, sr)

    def test_iterable(self):

        class M(Middleware):
            def iterable_hook(self, iterable):
                pass

        environ = {}
        sr = StartResponse()
        with raises(BadMiddleware):
            M(APP)(environ, sr)


def test_startresponse():
    """Test StartResponse gathers data reasonably.

    Tests depend on it, so if it fails then other tests are suspect
    """
    sr = StartResponse()
    status = "666 Malicious Application"
    headers = [('Frob', 'Ozzled')]
    sr(status, headers)
    assert sr.status == status
    assert sr.headers is headers

    cls = Exception
    instance = Exception('foo')
    traceback = None
    exc_info = (cls, instance, traceback)
    sr(status, headers, exc_info=exc_info)
    assert sr.status == status
    assert sr.headers is headers
    assert sr.exc_info == exc_info


class EnvironChanger(Middleware):
    """Middleware subclass which trivially changes environ as it goes to app.
    """
    key = "abcdefg"
    value = "whatever"

    def environ_hook(self, environ):
        environ[self.key] = self.value
        return environ


def test_change_environ():
    """environ_hook changes environ.
    """
    mwcls = EnvironChanger
    environ = {mwcls.key: 'something else'}
    sr = StartResponse()

    def app(environ, start_response):
        start_response("200 OK", [])
        if environ[mwcls.key] == mwcls.value:
            return ['OK']
        else:
            return ['nope']

    resp1 = app(environ, sr)
    resp2 = EnvironChanger(app)(environ, sr)
    assert resp1 != ['OK']
    assert resp2 == ['OK']


class StatusChanger(Middleware):
    """Middleware subclass which changes status given by app.
    """
    value = "777 Blah blah blah"

    def headers_hook(self, status, headers, exc_info=None):
        return self.value, headers, exc_info


def test_change_status():
    mwcls = StatusChanger
    environ = {}
    sr = StartResponse()

    def app(environ, start_response):
        start_response("200 OK", [])
        return ['OK']

    resp1 = app(environ, sr)
    assert resp1 == ['OK']
    assert sr.status != mwcls.value

    resp2 = mwcls(app)(environ, sr)
    assert resp2 == ['OK']
    assert sr.status == mwcls.value


class HeadersChanger(Middleware):
    """Middleware subclass which changes headers given by app.
    """
    key = "Content-Type"
    value = "text/shmushed"

    def headers_hook(self, status, headers, exc_info=None):
        new_headers = [(self.key, self.value)]
        return self.value, new_headers, exc_info


def test_change_headers():
    mwcls = HeadersChanger
    environ = {}
    sr = StartResponse()

    def app(environ, start_response):
        status = "200 OK"
        headers = []
        start_response(status, headers)
        assert start_response.status == "200 OK"
        assert start_response.headers == headers
        return ['OK']

    expected = (mwcls.key, mwcls.value)
    resp1 = app(environ, sr)
    assert resp1 == ['OK']
    assert expected not in sr.headers

    resp2 = mwcls(app)(environ, sr)
    assert resp2 == ['OK']
    assert expected in sr.headers


class DummyError(Exception):
    pass


class ExcinfoChanger(Middleware):
    """Middleware subclass which changes exc_info given by app.
    """
    cls = DummyError

    def headers_hook(self, status, headers, exc_info=None):
        real_exc_info = sys.exc_info()
        fake_exc_info = self.cls, self.cls("Broken Shoe"), real_exc_info[2]
        return status, headers, fake_exc_info


def test_change_excinfo():
    mwcls = ExcinfoChanger
    environ = {}
    sr = StartResponse()

    def app1(environ, start_response):
        """Does not throw exception, therefore exc_info should not be modified
        """
        start_response("200 No Errors", [])
        return ['Great']

    # sanity checks
    assert app1(environ, sr) == ['Great']
    assert mwcls(app1)(environ, sr) == ['Great']
    assert sr.status.startswith('200 ')
    # even though the middleware changes exc_info, it should not in this case
    # because there is no existing exception context
    assert sr.exc_info is None

    def app2(environ, start_response):
        """Throws exception, exc_info should be modified
        """
        try:
            1 / 0
        except ZeroDivisionError:
            start_response("500 Error", [], sys.exc_info())
            return ['OK']
        return ['Uh-oh']

    # app2 should behave as expected unwrapped, or test is invalid
    resp1 = app2(environ, sr)
    assert resp1 == ['OK']
    cls, instance, traceback = sr.exc_info
    assert cls == ZeroDivisionError

    # now make sure the exc_info is changed when wrapped
    resp2 = mwcls(app2)(environ, sr)
    assert resp2 == ['OK']
    cls, instance, traceback = sr.exc_info
    assert cls != ZeroDivisionError
    assert cls == mwcls.cls
    assert isinstance(instance, mwcls.cls)
