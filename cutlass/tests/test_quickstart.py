from multiprocessing import Process
import socket
import sys
try:
    from urllib2 import urlopen, URLError
except ImportError:
    from urllib.request import urlopen, URLError
from cutlass.quickstart import run

def as_latin1_bytes(iterable):
    for item in iterable:
        encoded = item.encode('latin-1')
        assert isinstance(encoded, bytes)
        yield encoded

def test_run():
    def app(environ, start_response):
        start_response("200 OK", [])
        return as_latin1_bytes(["Yoo", "yaz!"])

    output = sys.stdout
    port = 8209
    def doit():
        try:
            run(app, port=port)
        except socket.error:
            output.write('could not get a socket')
            return
    server_process = Process(target=doit)
    server_process.start()

    output.write("Opening connection...\n")
    while True:
        assert server_process.is_alive(), "server process died prematurely"
        try:
            output.write("a")
            remote = urlopen('http://localhost:%s' % port)
        except URLError:
            output.write("x")
            output.flush()
            continue
        break
    output.write("Have a connection, reading data...\n")
    output.flush()

    data = remote.read()
    output.write("Have data: %s\n" % data)
    try:
        output.write("asserting\n")
        assert data == b'Yooyaz!'
    finally:
        output.write("Halting...\n")
        server_process.terminate()
