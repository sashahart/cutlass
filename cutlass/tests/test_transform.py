import json
from pytest import raises
from cutlass.transform import ensure_iterable, transform, transformed


def test_ensure_iterable():
    assert list(ensure_iterable([1,2])) == [1, 2]

    # TypeError is used to detect non-iterables
    assert list(ensure_iterable(1)) == [1]

    # Do NOT suppress all TypeError
    with raises(TypeError):
        exploding_iterator = (None/1 for item in [1])
        [item for item in ensure_iterable(exploding_iterator)]


def ignore(*args, **kwargs):
    """Input transformer which removes all arguments.
    """
    return [], {}


def passer(*args, **kwargs):
    """Input transformer which does absolutely nothing, by taking any
    arguments and returning them as-is.
    """
    return args, kwargs


def unpacker(args, kwargs=None):
    """Input transformer which implicitly unpacks two arguments: it takes
    a list and a dict and returns the same as args/kwargs.
    """
    return args, kwargs or {}


class TestTransform(object):

    def test_no_args(self):
        """transform() errors if given no arguments.

        must test because both required and optional are keyword args, thus
        Python will not generate the signature TypeError if neither are
        supplied.
        """
        with raises(TypeError):
            @transform()
            def foo():
                pass

    def test_source_noinput(self):
        """Test expected convention for passing no arguments.

        This just verifies behavior against interface spec
        """
        @transform(source=ignore)
        def noinput():
            pass

        noinput(2, 3, 4, 9, foo="bar")

    def test_source_passthrough(self):
        """Test expected convention for passing arguments through.

        This just verifies behavior against interface spec
        """
        def add0(a, b):
            return a + b

        @transform(source=passer)
        def add1(a, b):
            return a + b

        assert add1(9999, 1) == 10000
        for i in range(-1000, 1000):
            assert add0(i, i) == add1(i, i)

    def test_source_unpacker(self):
        """Test expected convention for unpacking arguments.

        This just verifies behavior against interface spec
        """
        @transform(source=unpacker)
        def add2(a, b, sign=False):
            if sign:
                return a + b
            else:
                return -a - b

        assert add2([8, 8], {'sign': True}) == 16
        assert add2([8, 8], {'sign': False}) == -16

        @transform(source=[passer, passer, unpacker, passer, passer])
        def add3(a, b, sign=False):
            if sign:
                return a + b
            else:
                return -a - b
        assert add3([13, 7], {'sign': True}) == 20
        assert add3([13, 7], {'sign': False}) == -20

    def test_target_unpacking(self):
        """Verify the tuple unpacking behavior used to allow pipelines of
        multiple functions easily
        """
        def u1(arg):
            return (arg,)

        def u2(arg):
            return (arg, 2)

        def u3(arg1, arg2):
            return arg1 * 2, arg2 * 3

        @transform(target=u1)
        def foo():
            return 1

        assert foo() == (1,)

        @transform(target=[u1, u2])
        def bar():
            return 1

        assert bar() == (1, 2,)

        @transform(target=[u2, u3])
        def baz():
            return 10

        assert baz() == (10 * 2, 2 * 3)

    def test_source_badreturns(self):
        """Check that source functions returning weird tuples generate expected
        errors
        """

        def bad1():
            return (1,)

        def bad2():
            return (3, 4, 5,)

        def bad3():
            return None, {}

        def bad4():
            return [], None

        for fn in (bad1, bad2, bad3, bad4):
            exception = None
            try:
                @transform(source=fn)
                def example():
                    pass

                example()
            except (ValueError, TypeError, AttributeError) as e:
                exception = e
            if exception is None:
                raise AssertionError("didn't raise for %s" % fn)

    def test_target_simple(self):
        "Test simple one-parameter target functions"
        @transform(target=str)
        def add4(a, b):
            return a + b
        assert add4(1, 2) == "3"

        @transform(target=str.upper)
        def foo():
            return "yams"
        result = foo()
        assert result == "YAMS"

    def test_json(self):
        """Example with simple one-arg, one-return functions on both ends.

        There are lots of functions out there like this and it's nice to be able to
        use them without much fuss.
        """
        def double_dict(input_dict):
            "Solve a Python problem in Python style."
            output_dict = {}
            for key in input_dict:
                output_dict[key] = input_dict[key] * 2
            return output_dict

        @transform(source=json.loads, target=json.dumps)
        def double_dict_json1(input_dict):
            "Solve a JSON problem in Python style."
            return double_dict(input_dict)

        # Same deal, different syntax
        double_dict_json2 = transformed(double_dict, source=json.loads,
                                       target=json.dumps)

        def check(dd, ddj):
            python_dict = {'foo': 2, 'bar': 4}
            json_input = json.dumps(python_dict)
            json_output = ddj(json_input)
            assert dd(python_dict) == {'foo': 4, 'bar': 8}
            assert json.loads(json_output) == dd(python_dict)

        check(double_dict, double_dict_json1)
        check(double_dict, double_dict_json2)
