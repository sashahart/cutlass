def test_just_import():
    import cutlass
    import cutlass.wsgi
    import cutlass.routing
    import cutlass.transform
    import cutlass.request
    import cutlass.response
    #import cutlass.wsgi_to_cgi
    #import cutlass.validate

def test_toplevel():
    from cutlass import WSGI
    from cutlass import Dispatcher
    from cutlass import Cascade
    from cutlass import Request
    from cutlass import Response
    from cutlass import transform, transformed
    from cutlass import Cookie, Cookies
