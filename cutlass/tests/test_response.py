# coding: utf-8
from __future__ import unicode_literals
from pytest import raises
from cutlass.response import (
    HTTP_STATUS_CLASSES, HTTP10_STATUS_CODES, HTTP11_STATUS_CODES,
    Response, JSON
    )
import json

# RFC 2616 2.2; same used in RFC 1945 2.2
CONTROL_CHARACTERS = set([chr(i) for i in range(0, 32)] + [chr(127)])
SEPARATORS = set('()<>@,;:\\"/[]?={} \t')


try:
    basestring
except:
    basestring = str


def test_simple():
    r = Response()
    r.status = "400 Derp derp derp"
    assert r.status == "400 Derp derp derp"
    r.headers['Cache-Control'] = 'whatever'
    assert r.headers['cache-control'] == 'whatever'
    r.body = "Body is here"
    rendered = r.render()
    assert r.status.encode('iso-8859-1') in rendered
    assert 'whatever'.encode('iso-8859-1') in rendered
    assert r.body.encode('iso-8859-1') in rendered


def test_set_header():
    r = Response()
    assert 'foo' not in r.headers
    r.headers['foo'] = 'bar'
    assert r.headers['foo'] == 'bar'


def test_set_body():
    r = Response()
    assert not r.body
    r.body = "flup"
    assert r.body == "flup"


def test_set_iterable():
    r = Response()
    iterable_content = ["Ooh", "la", "la"]
    r.app_iter = iterable_content
    assert tuple([item for item in r.app_iter]) == \
            tuple([item for item in iterable_content])
    assert r.body == "".join(iterable_content)


def test_set_body_get_iterable():
    r = Response()
    r.body = "foobar"
    assert list(r.app_iter) == [b"foobar"]


def test_set_iter_get_body():
    r = Response()
    r.app_iter = [b"foo", b"bar"]
    assert r.body == "foobar"


def test_set_content_type():
    """Test that content type can be set and syncs across header and Response
    attrs.
    """
    r = Response()
    assert r.content_type == r.headers['Content-Type']
    sentinel = 'text/wobbled'
    assert r.content_type != sentinel

    r.content_type = sentinel
    assert r.content_type == sentinel
    assert r.headers['Content-Type'] == sentinel
    assert r.headers['content-type'] == sentinel

    sentinel2 = 'text/frobbled; charset=latin-1'
    r.headers['Content-Type'] = sentinel2
    assert r.headers['ConTenT-TyPe'] == sentinel2
    assert r.content_type == sentinel2
    assert r.charset == 'latin-1'
    assert r.mime_type == 'text/frobbled'


def test_sane_web_defaults():
    "Ensure that blank response is ready to go for typical web"
    r = Response()
    assert r.status.startswith("200 ")
    assert r.mime_type.startswith("text/html")
    assert r.charset.lower() in ('utf-8', 'utf8')


def test_error():
    from cutlass.response import Error
    ideal = (b"HTTP/1.1 500 Internal Server Error\r\n"
             b"Content-Type: text/html; charset=utf-8\r\n"
             b"Content-Length: 0\r\n\r\n")
    assert Error().render() == ideal


def test_redirect():
    from cutlass.response import Redirect
    url = "https://example.com"
    ideal = (b"HTTP/1.1 301 Moved Permanently\r\n"
             b"location: https://example.com\r\n"
             b"Content-Type: text/html; charset=utf-8\r\n"
             b"Content-Length: 0\r\n\r\n")
    assert Redirect(url).render() == ideal

    ideal = (b"HTTP/1.1 301 Moved Permanently\r\n"
             b"location: https://example.com\r\n"
             b"Content-Type: text/html; charset=utf-8\r\n"
             b"Content-Length: 0\r\n\r\n")
    assert Redirect(url, temporary=True).render() == ideal


def test_seeother():
    from cutlass.response import SeeOther
    url = "https://example.com"
    ideal = (b"HTTP/1.1 303 See Other\r\n"
             b"location: https://example.com\r\n"
             b"Content-Type: text/html; charset=utf-8\r\n"
             b"Content-Length: 0\r\n\r\n")
    assert SeeOther(url).render() == ideal


def test_notfound():
    from cutlass.response import NotFound

    ideal = (b"HTTP/1.1 404 Not Found\r\n"
            b"Content-Type: text/html; charset=utf-8\r\n"
            b"Content-Length: 0\r\n\r\n")
    #assert NotFound.render() == ideal
    assert NotFound().render() == ideal


def test_forbidden():
    from cutlass.response import Forbidden

    ideal = (b"HTTP/1.1 403 Forbidden\r\n"
            b"Content-Type: text/html; charset=utf-8\r\n"
            b"Content-Length: 0\r\n\r\n")
    #assert Forbidden.render() == ideal
    assert Forbidden().render() == ideal


def _test_contains_function(function, positives):
    ref = sorted(list(positives))
    for char in ref:
        print(repr(char))
        assert function(char)
        assert function("a" + char + "b")
        assert function(char + "b")
        assert function("a" + char)
    from string import ascii_letters, digits
    assert not function(ascii_letters + digits)


def test_contains_control_characters():
    from cutlass.response import contains_control_characters
    assert len(CONTROL_CHARACTERS) > 1
    _test_contains_function(contains_control_characters,
                            CONTROL_CHARACTERS)
    from string import punctuation
    assert not contains_control_characters(punctuation)


def test_contains_separators():
    from cutlass.response import contains_separators
    assert len(SEPARATORS) > 1
    _test_contains_function(contains_separators,
                            SEPARATORS)


def test_valid_status_code():
    from cutlass.response import valid_status_code
    cases = [
        (None, False),
        ("", False),
        (0, False),
        ("1", False),
        ("12", False),
        ("20 ", False),
        (42, False),
        ("40", False),
        ("5055", False),
        ('200', True),
        (200, True),
        (1000, False),
        (999, False),
        (600, False),
    ]
    for code, expected in cases:
        print(code, expected)
        obtained = valid_status_code(code)
        assert obtained == expected

    # Sanity check that HTTP/1.1 codes not valid for 1.0
    assert valid_status_code(203, version='1.1')
    assert not valid_status_code(203, version='1.0')

    # Raise error if we ask for weird versions
    for version in [
        '0',
        'x5',
        '2.0'
        ]:
        with raises(Exception):
            valid_status_code(200, version=version)


def test_valid_status_string():
    from cutlass.response import valid_status_string
    valid_cases = [
        "200 OK",
    ]
    invalid_cases = [
        None,
        "",
        "    ",
        0,
        200,
        "200",
        "20 OK",
        "20    ",
        " 200",
        " 200 OK",
        "000 Nothing",
        "999 Enormous",
        "9999 Over",
    ]
    for valid_status in valid_cases:
        assert valid_status_string(valid_status) == True
    for invalid_status in invalid_cases:
        assert valid_status_string(invalid_status) == False

    assert valid_status_string("200 ", require_reason=False) == True
    assert valid_status_string("200 ", require_reason=True) == False


def test_parse_content_type():
    from cutlass.response import parse_content_type

    # Parse everything Python knows about
    from mimetypes import types_map
    known_types = types_map.values()
    for known_type in known_types:
        obtained_mime_type, obtained_parms = parse_content_type(known_type)
        assert obtained_parms == {}
        assert obtained_mime_type == known_type

    cases = [
        ('',
            '', {}),
        ('text/plain; what=ever',
            'text/plain', {'what': 'ever'}),
        ('text/plain;what=ever',
            'text/plain', {'what': 'ever'}),
        ('text/plain; what=ever; something=else',
            'text/plain', {'what': 'ever', 'something': 'else'}),
        ('text/plain; what=ever;something=else',
            'text/plain', {'what': 'ever', 'something': 'else'}),
        ('text/plain;what=ever;something=else',
            'text/plain', {'what': 'ever', 'something': 'else'}),
        ('text/html; charset=utf-8', 'text/html', {'charset': 'utf-8'}),
        ('application/atom+xml', 'application/atom+xml', {}),
    ]
    for inp, expected_mime_type, expected_parms in cases:
        obtained_mime_type, obtained_parms = parse_content_type(inp)
        assert obtained_mime_type == expected_mime_type
        assert obtained_parms == expected_parms

    for inp, expected_mime_type, expected_parms in cases:
        obtained_mime_type, obtained_parms = parse_content_type(
                                                inp, no_dups=False)
        assert obtained_mime_type == expected_mime_type
        for key in expected_parms:
            expected_parms[key] = [expected_parms[key]]
        assert obtained_parms == expected_parms

    dup_cases = [
        ('foo=bar; foo=bar',
         {'foo': ['bar', 'bar']}
        ),
        ('foo=bar; foo=baz',
         {'foo': ['bar', 'baz']}
        ),
        ('foo=baz; foo=bar',
         {'foo': ['baz', 'bar']}
        ),
    ]
    for inp, expected in dup_cases:
        mime_type, parms = parse_content_type("text/plain; " + inp,
                                              no_dups=False)
        assert parms == expected

    # dedupe, take the last - this could change
    nodup_cases = [
        ('what=ever; what=ever', {'what': 'ever'}),
        ('what=ever; what=even', {'what': 'even'}),
        ('a=b; a=c; a=d', {'a': 'd'}),
    ]
    for inp, expected in nodup_cases:
        mime_type, parms = parse_content_type("text/plain; " + inp,
                                              no_dups=True)
        assert parms == expected


def test_render_content_type():
    from cutlass.response import render_content_type
    from mimetypes import types_map
    known_types = types_map.values()
    for known_type in known_types:
        assert render_content_type(known_type) == known_type

    cases = [
        ('text/plain', None, None, 'text/plain',),
        ('text/plain', 'utf-8', None,
            'text/plain; charset=utf-8',),
        ('text/plain', None, {'foo': 'bar'},
            'text/plain; foo=bar',),
        ('text/plain', 'utf-8', {'foo': 'bar'},
            'text/plain; charset=utf-8; foo=bar',),
    ]
    for mime_type, charset, parms, expected in cases:
        result = render_content_type(mime_type, charset=charset, parms=parms)
        assert result == expected


def test_correct_headers():
    from cutlass.response import correct_headers
    from wsgiref.headers import Headers
    cases = [
        ({}, {}),
    ]
    for inp, outp in cases:
        inp = Headers(list(inp.items()))
        outp = Headers(list(outp.items()))

    assert correct_headers({}, mime_type='text/plain') == \
            {'Content-Type': 'text/plain'}
    assert correct_headers({}, mime_type='text/plain', charset='utf-8') == \
            {'Content-Type': 'text/plain; charset=utf-8'}
    # Don't override if good stuff already present
    assert correct_headers(
                {'Content-Type': 'text/html'},
                mime_type='text/plain'
            ) == {'Content-Type': 'text/html'}
    assert correct_headers(
                {'Content-Type': 'text/html; charset=utf-8'},
                mime_type='text/plain',
                charset='latin-1'
            ) == {'Content-Type': 'text/html; charset=utf-8'}
#   # TODO: do override if bad stuff present
#   assert correct_headers({'Content-Type': 'kurqemp'},
#           mime_type='text/plain') == \
#           {'Content-Type': 'text/plain'}
#   assert correct_headers({'Content-Type': 'text/html; charset=utf-8'},
#           mime_type='text/plain', charset='latin-1') == \
#           {'Content-Type': 'text/html; charset=utf-8'}


def test_render_header():
    from cutlass.response import render_header
    assert render_header() == b"HTTP/1.1 200 OK\r\n\r\n"
    assert render_header(status="974 Florb") == \
            b"HTTP/1.1 974 Florb\r\n\r\n"
    assert render_header(version='9.991') == \
            b"HTTP/9.991 200 OK\r\n\r\n"
    assert render_header(headers={
            'foo': 'bar', 'baz': 'bam'
            }) in \
    (b"HTTP/1.1 200 OK\r\nbaz: bam\r\nfoo: bar\r\n\r\n",
     b"HTTP/1.1 200 OK\r\nfoo: bar\r\nbaz: bam\r\n\r\n")


def test_render_body():
    # reminder: this is not limited to WSGI,
    # it needs to be able to deal with various charsets
    # besides latin-1
    from cutlass.response import render_body, DEFAULT_CHARSET
    assert render_body("") == b""

    class Stringable(object):
        def __str__(self):
            return "awooga"
    assert render_body(Stringable()) == b"awooga"
    # Make a Python unicode string to play with
    # (in Python 3 must be str, no u'')
    president = "Felipe Calderón"
    result = render_body(president)
    assert result == president.encode(DEFAULT_CHARSET)
    accent_o = 'ó'
    assert accent_o in president
    for char in result:
        assert char != accent_o
    result = render_body(president, charset='latin-1')
    assert result == president.encode('latin-1')
    assert result != president.encode(DEFAULT_CHARSET)


def test_well_formed_status_code_tables():
    for letter in "12345":
        assert letter in HTTP_STATUS_CLASSES
        assert HTTP_STATUS_CLASSES[letter]
    # Sanity check that there is something meaningful there
    for code in (200, 400, 404, 500):
        assert code in HTTP10_STATUS_CODES
    # Sanity check that we map ints to 2-tuples of non-empty strings
    for table in [HTTP10_STATUS_CODES, HTTP11_STATUS_CODES]:
        for key, value in table.items():
            reason, help = value
            print(key, reason, help)
            assert isinstance(key, int)
            assert reason
            assert help


def test_get_reason():
    from cutlass.response import get_reason
    assert get_reason(200) == 'OK'
    assert get_reason(999) == ''


def test_render_response():
    from cutlass.response import render_response
    assert render_response() == b"HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n"
    assert render_response(body="fruits") == \
            b"HTTP/1.1 200 OK\r\nContent-Length: 6\r\n\r\nfruits"


def test_parse_protocol():
    from cutlass.response import parse_protocol
    assert parse_protocol("HTTP/1.1") == ("HTTP", "1", "1")
    assert parse_protocol("SNOP/9.23") == ("SNOP", "9", "23")


class TestJson(object):

    def test_simple(self):
        data = {'a': 'b'}
        resp = JSON(data, chaff='', obj_key=None)
        assert resp.body == '{"a": "b"}'
        assert resp.content_type == "application/json; charset=utf-8"
        assert resp.mime_type == "application/json"
        assert resp.charset == "utf-8"

    def test_attachment(self):
        data = {'a': 'b'}
        resp = JSON(data)
        assert resp.headers['cOntent-dIsposition'] == 'attachment'
        resp = JSON(data, attachment=False)
        assert 'cOntent-dIsposition' not in resp.headers

    def test_obj_key(self):
        data = {'a': 'b'}
        obj_key = 'blah'
        resp = JSON(data, chaff='', obj_key=obj_key)
        assert resp.body == '{"%s": {"a": "b"}}' % obj_key

    def test_chaff(self):
        chaff = "sentinel"
        data = {'a': 'b'}
        resp = JSON(data, chaff=chaff)
        assert resp.body.startswith(chaff)
        rest = resp.body[len(chaff):]
        assert rest == '{"a": "b"}'

    def test_dict_of_lists(self):
        seq = {'a': ['b', 'c'], 'b': ['d', 'e']}
        resp = JSON(seq, chaff='')
        assert json.loads(resp.body) == seq

    def test_list_of_dicts(self):
        seq = [{'a': 'b'}, {'c': 'd'}]
        resp = JSON(seq, chaff='')
        assert json.loads(resp.body) == seq
