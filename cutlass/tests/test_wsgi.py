from pytest import raises
from cutlass.wsgi import (
        WSGI, unpack_response, run_runner, default_runner,
        default_error_handler, DefaultIterableWrapper,
        HandlerError)
from cutlass.test import LogIntercept, StartResponse, check_iterable
bytestring = type(b'')
START_RESPONSE = lambda status, headers, exc_info=None: None


def _run_handler(handler, environ, start_response):
    # Exercise the defaults as used in @WSGI
    result = run_runner(default_runner, handler, environ, start_response,
                        error_handler=default_error_handler,
                        iterable_wrapper=DefaultIterableWrapper)
    return result


def fatal_iterator():
    raise MemoryError("oh no...")
    yield "kaboom"


def bad_iterator():
    raise TypeError("oh no!")
    yield "blam"


def test_run_handler_sanity():
    "Quick check that _run_handler is exercising the right bits"
    w = WSGI()
    assert w.error_handler == default_error_handler
    assert w.iterable_wrapper == DefaultIterableWrapper


def test_simple():
    @WSGI
    def main1(environ):
        return "main1"

    iterable = main1({}, lambda status, headers, exc_info=None: None)
    assert isinstance(iterable, DefaultIterableWrapper)
    check_iterable(iterable)
    assert tuple(iterable) == tuple([b"main1"])


def test_default_error_handler():
    environ = {}
    start_response = StartResponse()

    def check(result):
        check_iterable(result)
        assert tuple(result) == tuple([b""])
        assert start_response.status[-1].startswith("500 ")
        assert len(start_response.headers[-1]) == 0
    # Call inside exception, check for traceback
    with LogIntercept() as log:
        try:
            raise Exception("dummy")
        except:
            result = default_error_handler(environ, start_response)
            check(result)
        assert 'Traceback' in log.value()
        assert 'Exception: dummy' in log.value()
    # Call outside of exception
    result = default_error_handler(environ, start_response)
    check(result)

    # Call via default_runner
    def handler(environ):
        raise Exception("duh")
    result = _run_handler(handler, environ, start_response)
    check(result)


class C(object):
    def __init__(self, **kwargs):
        for kwarg in kwargs:
            if not hasattr(self, kwarg):
                setattr(self, kwarg, kwargs[kwarg])


def test_unpack_response():
    # Has to be a status attribute and it has to be splittable
    for item in [
            # no attribute
            C(),
            # not splittable
            C(status=2),
            C(status=None),
            # won't unpack properly
            C(status=""),
            C(status="bla blb blc"),
            # but these should all split.
            C(status="duh"),
            C(status="bla "),
            C(status="200"),
            C(status="200 OK"),
            ]:
        try:
            result = unpack_response(item)
        except Exception as e:
            print(e, e.__class__)


def test_default_runner_good_handlers():
    """Smoke test with several kinds of reasonable handlers.
    """

    def good_handler(environ):
        return ["I'm a good handler"]

    def another_good_handler(environ):
        return ["Oh", "Boy!"]

    def yay_handler(environ):
        return (str(i) for i in range(0, 100))

    def begrudgingly_accepted_handler(monkeys):
        return ["", "Looks fine to me", "", ""]

    local_items = list(locals().items())
    for handler_name, handler in local_items:
        environ = {}
        start_response = StartResponse()
        result = _run_handler(handler, environ, start_response)
        issues = list(check_iterable(result))
        assert not issues
        assert start_response.call_count > 0
        issues = start_response.all_issues()
        if issues:
            print("Found start_response issues in %s:\n\t" % handler_name
                  + "\n\t".join(issues))
        assert not issues



def test_default_runner_bad_handlers():
    """Smoke test with several kinds of bad handlers, to make sure
    run_runner/default_runner does not die or let big WSGI problems go.
    """

    def zen_handler():
        "Oops, no argument and no return value"
        pass

    def wsgi_handler(environ, start_response):
        """@WSGI doesn't wrap stuff which already implements the WSGI protocol.
        """
        start_response("200 OK", [])
        return ["Correct, but not as a handler wrapped by @WSGI"]

    # TODO: ensure that all the goodness of @WSGI besides signature
    # transformation and delegation of start_response calls is available
    # separately of @WSGI!

    def wsgi_lite_handler(environ):
        """@WSGI doesn't do 3-tuples because this is impossible to distinguish
        from iterators in the general case. If a Rack-style protocol is
        supported, it should be with a different decorator.
        """
        return "200 OK", [], "Stuff"

    def comatose_handler(environ):
        "Oops, no returned string/iterable/response object"
        pass

    def doing_it_wrong_handler(environ):
        return 200

    def returns_numbers(environ):
        "Returns an iterable but totally wrong type"
        return [9, 2, 5, 1, 9.2]

    def moribund_handler(environ):
        "Generate exception inside returned iterator"
        failing_iterator = (str(1 / (i % 5)) for i in range(1, 6))
        return failing_iterator

    local_items = list(locals().items())
    for handler_name, handler in local_items:
        environ = {}
        start_response = StartResponse()
        result = _run_handler(handler, environ, start_response)
        check_iterable(result)
        assert start_response.call_count > 0
        issues = start_response.all_issues()
        if issues:
            print(("Found start_response issues while running %s:\n\t"
                  % handler_name) + "\n\t".join(issues))
        assert not issues


def test_handlererror():
    try:
        raise HandlerError('')
    except Exception as e:
        assert str(e) == ''
    try:
        raise HandlerError("hi")
    except Exception as e:
        assert str(e) == "hi"


def test_wsgi_error_handler():

    def handler(environ, start_response):
        handler.called = True
    handler.called = False

    @WSGI(error_handler=handler)
    def foo(environ):
        raise Exception("invoke the error handler!")

    foo({}, START_RESPONSE)
    assert handler.called == True


def test_wsgi_no_error_handler():
    @WSGI(error_handler=None)
    def foo(environ):
        raise Exception("invoke the error handler!")

    with raises(Exception):
        foo({}, START_RESPONSE)


def test_wsgi_memoryerror():
    # nasty errors should bubble up even with error handler.
    @WSGI
    def handler(environ):
        raise MemoryError("bad stuff going down")

    with raises(MemoryError):
        handler({}, START_RESPONSE)


def test_wsgi_uncallable_handler():
    foo = WSGI(None)
    with raises(HandlerError):
        foo({}, START_RESPONSE)


def test_wsgi_uncallable_runner():
    foo = WSGI(runner=None)
    with raises(HandlerError):
        foo({}, START_RESPONSE)


def test_wsgi_errorception():
    def error_handler1(environ, start_response):
        raise MemoryError("untrustworthy error handler")

    def error_handler2(environ, start_response):
        raise Exception("not obviously fatal error")

    @WSGI(error_handler=error_handler1)
    def foo(environ):
        raise Exception("throw error so error_handler is called")

    @WSGI(error_handler=error_handler2)
    def bar(environ):
        raise Exception("throw error")

    # Fatal error raised inside error handler bubbles up
    with raises(MemoryError):
        foo({}, START_RESPONSE)

    # Other error raised inside error handler -> default 500
    start_response = StartResponse()
    result = bar({}, start_response)
    assert start_response.status[0].startswith('500')


def test_wsgi_nowrapper():
    sentinel = [1, 2, 3]

    @WSGI(iterable_wrapper=None)
    def foo(environ):
        return sentinel

    @WSGI
    def bar(environ):
        return sentinel

    assert foo({}, START_RESPONSE) is sentinel
    assert bar({}, START_RESPONSE) is not sentinel


def test_wrapper_error_defaulthandler():
    """Test behavior of default error handler
    """

    sr = StartResponse()
    iterable = DefaultIterableWrapper(bad_iterator(), {}, sr)
    [item for item in iterable]
    assert sr.status[0].startswith("500 ")


def test_wrapper_error_customhandler():
    """Test that error handler can be overridden
    """
    status = "200 OK"
    text = b'No, everything is just fine'

    def fine_error_handler(environ, start_response):
        start_response(status, [])
        return [text]

    sr = StartResponse()
    iterable = DefaultIterableWrapper(bad_iterator(), {}, sr,
            error_handler=fine_error_handler)
    assert [item for item in iterable] == [text]
    assert len(sr.status) == 1
    assert sr.status[0] == "200 OK"


def test_wrapper_error_nohandler():
    """Test what happens when error_handler is suppressed
    """
    sr = StartResponse()
    iterable = DefaultIterableWrapper(bad_iterator(), {}, sr,
            error_handler=None)
    with raises(TypeError):
        [item for item in iterable]


def test_wrapper_error_badhandler():
    """Test recovery from a bad error handler.
    """
    def bad_error_handler(environ, start_response):
        raise AttributeError(
            "If this bubbles up, then a bad error handler raises "
            "an exception instead of going to default.")

    sr = StartResponse()
    iterable = DefaultIterableWrapper(bad_iterator(), {}, sr,
            error_handler=bad_error_handler)
    [item for item in iterable]
    assert sr.status[0].startswith("500 ")


def test_wrapper_error_awfulhandler():
    """Test appropriate non-recovery from fatal errors in error handler.
    """
    def awful_error_handler(environ, start_response):
        raise MemoryError("awooga")

    sr = StartResponse()
    iterable = DefaultIterableWrapper(bad_iterator(), {}, sr,
            error_handler=awful_error_handler)
    with raises(MemoryError):
        [item for item in iterable]


def test_wrapper_error_fatal():
    """Test appropriate non-recovery from fatal errors in iterator itself.
    """
    iterable = DefaultIterableWrapper(fatal_iterator(), {}, START_RESPONSE)
    with raises(MemoryError):
        [item for item in iterable]


def test_wrapper_closes():
    """Make sure that wrapper closes wrapped iterable when IT is asked to
    close, but not otherwise.
    """

    class Dummy(object):
        def __init__(self):
            self.closed = False

        def __iter__(self):
            return ("blah" for item in [0])

        def close(self):
            self.closed = True

    dummy = Dummy()
    iterable = DefaultIterableWrapper(dummy, {}, START_RESPONSE)
    # Not closed when we haven't even started iterating
    assert dummy.closed == False
    yielded = [item for item in iterable]
    # Now there is data, but close should not be autocalledj
    assert iterable.yield_count == 1
    assert list(yielded) == [b"blah"]
    assert dummy.closed == False
    # Now 'server' closes it and it is closed
    iterable.close()
    assert dummy.closed == True
