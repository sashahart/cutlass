#!/usr/bin/env python

from cutlass.routing import dispatch, Dispatcher, RegexMapper, FriendlyMapper
from cutlass.routing import Cascade, BadHandlerError, BadPatternError
from cutlass.routing import no_trailing_slash, ensure_trailing_slash
from cutlass.routing import consume_prefix
from cutlass.test import StartResponse
from py.test import raises

# TODO: need a lot more dispatch/dispatcher tests.

SENTINEL_STATUS = "700 Nope"
SENTINEL_BODY = "thingy"


def thing(environ, start_response):
    thing.status = SENTINEL_STATUS
    thing.body = SENTINEL_BODY
    start_response(thing.status, [])
    return [thing.body]


def check_results(result, start_response):
    assert start_response.call_count > 0
    assert len(start_response.status) == 1
    assert not start_response.status[0].startswith('404')
    assert start_response.status[0] == thing.status
    assert tuple(result) == (thing.body,)

# Handlers for exercising Cascade


def normal_handler(environ, start_response):
    start_response("200 OK", [('Content-Type', 'text/plain')])
    return ["Normal response"]


def bad_handler(environ, start_response):
    return


def fourohfour_handler(environ, start_response):
    start_response("404 Not Found", [])
    return "404: Not Found"


class TestCascade(object):

    def test_halts_on_success(self):
        class Dummy(object):
            pass
        dummy = Dummy()
        dummy.oops = False

        def should_never_run(environ, start_response):
            dummy.oops = True
        environ = {}
        start_response = lambda status, headers, exc_info=None: None
        c = Cascade(normal_handler, should_never_run)
        assert c(environ, start_response) == ["Normal response"]
        assert not dummy.oops

    def test_skips_404(self):
        c = Cascade(fourohfour_handler, fourohfour_handler, normal_handler)
        environ = {}
        start_response = lambda status, headers, exc_info=None: None
        assert c(environ, start_response) == ["Normal response"]

    def test_barfs_on_bad_handler(self):
        c = Cascade(bad_handler)
        environ = {}
        start_response = lambda status, headers, exc_info=None: None
        with raises(BadHandlerError):
            c(environ, start_response)

    def test_default_404(self):
        c = Cascade()
        environ = {}
        start_response = StartResponse()
        c(environ, start_response)
        assert start_response.call_count > 0
        assert len(start_response.status) == 1
        assert start_response.status[0].startswith("404 ")


def request(arg):
    method, path = arg.split()
    return {'REQUEST_METHOD': method,
            'PATH_INFO': path}

dummy_sr = lambda status, headers, exc_info=None: None


def response_body(path, handler):
    return list(
            handler({'REQUEST_METHOD': 'GET', 'PATH_INFO': path}, dummy_sr))[0]


def _full_body(iterable):
    return "".join(iterable)


class TestFriendlyMapper(object):
    def test_canonicalize_complaint(self):
        def strip(s):
            return s.strip('/')
        mapper = FriendlyMapper(canonicalize=strip)
        mappings = [('foo/', lambda:  None)]
        with raises(BadPatternError):
            mapper.add(mappings)

    def test_reverse(self):

        dispatcher = Dispatcher(mapper=FriendlyMapper())

        @dispatcher.GET('/foo')
        def foo():
            pass

        assert dispatcher.reverse(foo) == '/foo'

        @dispatcher.GET('/dummy/{uuid}')
        def dummy_handler():
            pass

        assert dispatcher.reverse(dummy_handler, uuid="whatever") == \
                "/dummy/whatever"

    def test_warn_non_delimited(self):

        def particular(environ, start_response):
            pass

        mapper = FriendlyMapper()
        mapper.add([
            ('GET', '/y{a}/{b}', particular),
            ])
        result = mapper.map({'REQUEST_METHOD': 'GET',
                             'PATH_INFO': '/yfoo/bar'})
        assert result == (particular, [], {'a': 'foo', 'b': 'bar'})
        result = mapper.map({'REQUEST_METHOD': 'GET',
                             'PATH_INFO': '/xfoo/bar'})
        assert result[0] is None
        with raises(BadPatternError):
            mapper.add([
                ('GET', '/x{a}{b}', particular),
                ])
        mapper.add([
            ('GET', '/x{a}/{b}', particular),
            ])
        result = mapper.map({'REQUEST_METHOD': 'GET',
                             'PATH_INFO': '/xfoo/bar'})
        assert result == (particular, [], {'a': 'foo', 'b': 'bar'})


class TestRegexMapper(object):

    def test_dispatch(self):
        "Test actual use with dispatch()"
        mapper = RegexMapper([
                ("GET", "/wh(?P<zonk>.+)", thing),
                ("GET", "(.+)", thing),
            ])
        environ = request('GET /whatever')
        start_response = StartResponse()
        result = dispatch(environ, start_response,
            mapper=mapper)
        check_results(result, start_response)

    def test_dispatcher(self):
        "Test actual use with Dispatcher()"
        environ = request('GET /whatever')
        start_response = StartResponse()
        dispatcher = Dispatcher(
            mapper=RegexMapper([
                ("GET", "/wh(?P<zonk>.+)", thing),
                ("GET", "(.+)", thing),
            ])
            )
        result = dispatcher(environ, start_response)
        check_results(result, start_response)

    def test_map(self):

        def foo():
            pass

        def bar():
            pass

        def baz():
            pass

        mapper = RegexMapper([
                ('GET', '/foo.*', foo),
                ('POST', '/.*bar', bar),
                ('*', '/baz', baz),
            ])

        simple_cases = [
                    ('GET /bugs', None),
                    ('GET /foobles', foo),
                    ('POST /whabar', bar),
                    ('GET /whabar', None),
                    ('TRACE /baz', baz),
                    ]
        for req, expected in simple_cases:
            result = mapper.map(request(req))
            assert result[0] == expected

    def test_reverse(self):

        dispatcher = Dispatcher(mapper=RegexMapper())

        @dispatcher.GET('/foo')
        def foo():
            pass

        assert dispatcher.reverse(foo) == '/foo'

        @dispatcher.GET('/dummy/(?P<uuid>.+)')
        def dummy_handler():
            pass

        assert dispatcher.reverse(dummy_handler, uuid="whatever") == \
                "/dummy/whatever"


class TestDispatcher(object):

    def test_route_decorator(self):
        d = Dispatcher()
        dec = d.route_decorator('GET', 'POST')

        @dec('/a')
        def a(environ, start_response):
            return [b'a']

        @dec('/b')
        def b(environ, start_response):
            return [b'b']

        def sr(status, headers, exc_info=None):
            sr.status = status
            sr.headers = headers
            sr.exc_info = exc_info

        assert d(request('GET /a'), sr) == [b'a']
        assert d(request('GET /b'), sr) == [b'b']
        assert d(request('POST /a'), sr) == [b'a']
        assert d(request('POST /b'), sr) == [b'b']
        d(request('GET /'), sr)
        assert sr.status.startswith('404')
        d(request('GET /a'), sr)
        assert sr.status.startswith('404')

    def test_accepts_2tuple(self):
        def ok1(environ, start_response):
            start_response("200 OK", [])
            return ["OK1"]

        def ok2(environ, start_response):
            start_response("200 OK", [])
            return ["OK2"]

        dispatcher = Dispatcher(
            ('/ok1', ok1),
            ('/ok2', ok2),
            )
        assert response_body('/ok1', dispatcher) == 'OK1'
        assert response_body('/ok2', dispatcher) == 'OK2'

    def test_prefix(self):
        """Normal case for both forward and reverse mapping
        """
        sentinel = ["barbar"]
        def bar(environ, start_response):
            return sentinel
        d = Dispatcher(('/bar', bar), prefix='/foo')
        env = {'PATH_INFO': '/foo/bar', 'REQUEST_METHOD': 'GET'}
        sr = StartResponse()
        assert d(env, sr) == sentinel
        assert d.reverse(bar) == '/foo/bar'


def test_no_trailing_slash():
    f = no_trailing_slash
    cases = [
            ('/foo', '/foo'),
            ('/foo/', '/foo'),
            ('/foo/bar/baz', '/foo/bar/baz'),
            ('/foo/bar/baz/', '/foo/bar/baz'),
            ]
    for inp, expected in cases:
        assert f(inp) == expected


def test_ensure_trailing_slash():
    f = ensure_trailing_slash
    cases = [
            ('/foo', '/foo/'),
            ('/foo/', '/foo/'),
            # multiple path segments
            ('/foo/bar/baz', '/foo/bar/baz/'),
            ('/foo/bar/baz/', '/foo/bar/baz/'),
            # don't put trailing slash on filename looking things
            ('/foo.html', '/foo.html'),
            ('/bar/foo.html', '/bar/foo.html'),
            # but don't suppress trailing slash if dots in segments before last
            ('/a.b/x', '/a.b/x/'),
            ('/a.b/x/', '/a.b/x/'),
            ]
    for inp, expected in cases:
        assert f(inp) == expected


def test_canonical_noredirect():
    d = Dispatcher(
            ('/foo', thing),
        canonicalize=no_trailing_slash,
        canonical_redirect=False)

    sr = StartResponse()
    body = d(request('GET /foo'), sr)
    assert sr.status[0] == SENTINEL_STATUS
    assert SENTINEL_BODY in _full_body(body)

    sr = StartResponse()
    body = d(request('GET /foo/'), sr)
    assert sr.status[0] == SENTINEL_STATUS
    assert SENTINEL_BODY in _full_body(body)


def test_canonical_redirect():
    d = Dispatcher(
            ('/foo', thing),
        canonicalize=no_trailing_slash,
        canonical_redirect=True)

    sr = StartResponse()
    body = d(request('GET /foo'), sr)
    assert sr.status[0] == SENTINEL_STATUS
    assert SENTINEL_BODY in _full_body(body)

    sr = StartResponse()
    body = d(request('GET /foo/'), sr)
    assert sr.status[0].startswith("30")


class TestConsumePrefix(object):

    def result(self, prefix, path):
        d = {'PATH_INFO': path, 'SCRIPT_NAME': ''}
        consume_prefix(prefix, d)
        return (d['SCRIPT_NAME'], d['PATH_INFO'])

    def test_unmatched_prefix(self):
        "No changes if prefix did not match"
        assert self.result('/admin', '/foo/bar') == ('', '/foo/bar')

    def test_empty_prefix(self):
        "No change if prefix is ''"
        assert self.result('', '/foo') == ('', '/foo')

    def test_normal(self):
        assert self.result('/admin', '/admin/stuff') == ('/admin', '/stuff')
        # also check that we retain trailing slash on path.
        assert self.result('/admin', '/admin/stuff/') == ('/admin', '/stuff/')

    def test_consume_all(self):
        assert self.result('/admin', '/admin') == ('/admin', '')
        # also check that trailing slash is kept
        assert self.result('/admin', '/admin/') == ('/admin', '/')
