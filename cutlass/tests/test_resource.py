from cutlass.routing import resource, Resource
from cutlass.test import callable

def quickapp(name, status="200 OK", headers=None):
    if headers is None:
        headers = []

    def app(environ, start_response):
        start_response(status, headers)
        return [name]
    return app


foo = quickapp(b'foo')
baz = quickapp(b'baz')
bam = quickapp(b'bam')


def test_sanity_quickapps():
    """Sanity check the quickapps
    """
    sr = lambda s, h, e=None: None
    environ = {'REQUEST_METHOD': 'GET'}
    assert foo(environ, sr) == [b'foo']
    assert baz(environ, sr) == [b'baz']


def sr(status, headers, exc_info=None):
    sr.status = status
    sr.headers = headers
    sr.exc_info = exc_info


def test_sanity_sr():
    """Sanity check sr
    """
    sr("200 Blurb", [('Frob', 'bag')])
    assert sr.status == "200 Blurb"
    assert sr.headers == [('Frob', 'bag')]
    assert sr.exc_info == None
    sr("100 Whatever", [], "stuff")
    assert sr.exc_info == "stuff"


class TestResource(object):

    def setup_method(self, method):
        self.App = resource(GET=foo, POST=baz)

    def test_type(self):
        App = self.App
        assert issubclass(App, Resource)
        assert callable(App)

    def test_normal(self):
        App = self.App

        # GET maps to foo
        environ = {'REQUEST_METHOD': 'GET'}
        iterable = App(environ, sr)
        assert list(iterable) == [b'foo']

        # POST maps to baz
        environ = {'REQUEST_METHOD': 'POST'}
        iterable = App(environ, sr)
        assert list(iterable) == [b'baz']

    def test_add(self):
        App = self.App

        # Nothing mapped to PUT (yet)
        environ = {'REQUEST_METHOD': 'PUT'}
        iterable = App(environ, sr)
        assert sr.status.startswith('405 ')

        # But add it and then it works
        App.add(PUT=bam)
        iterable = App(environ, sr)
        assert list(iterable) == [b'bam']

    def test_subclass(self):
        class App(Resource):
            handlers = {
                'GET': foo,
                'PUT': baz,
            }
        environ = {'REQUEST_METHOD': 'PUT'}
        iterable = App(environ, sr)
        assert list(iterable) == [b'baz']
        environ = {'REQUEST_METHOD': 'GET'}
        iterable = App(environ, sr)
        assert list(iterable) == [b'foo']
