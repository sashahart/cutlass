"""Shim to let users import directly from a clone of the repo.

In many cases it will be cleanest to install the package using e.g. 'pip
install', 'pip install -e', or 'python setup.py install'. These should
allow you to install from PyPI, a tarball or a local clone of the repo.
And installing into a virtualenv keeps the dependencies for the project
separate from those for other projects (so, e.g., you can use different
versions of libraries and/or different Python interpreters). To automate
the installation of cutlass you can put it in a requirements.txt or list
it as a dependency in your project's setup.py, etc. Arguably this is the
correct practice.

But in some cases, users may prefer to put a clone of the repo under
their project directory, and import directly from the repo root.
To illustrate::

    myproject/
        myapp.py
        cutlass/
            setup.py
            cutlass/
                request.py
                ...

This file allows myapp.py to use 'from cutlass import routing' and
get the same module that you would get if cutlass were installed
on the current PYTHONPATH. This is an alternate way to isolate
project dependencies while also adding them to your project's
source control (either as files or as a submodule/subrepo).
"""
import sys
import os.path
sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)))
if 'cutlass' in sys.modules:
    del sys.modules['cutlass']
import cutlass
